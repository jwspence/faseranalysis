#include "TTree.h"
#include "TH1D.h"
#include "AtlasLabels.h"
#include "AtlasLabels.C"
#include "AtlasStyle.h"
#include "AtlasStyle.C"
#include "AtlasUtils.h"
#include "AtlasUtils.C"

void js_plot_final()
{
  SetAtlasStyle();

  //const int N_plane=8;
  //double target_z[8]={0, 50, 909.04, 959.04, 1009.04, 1059.04, 1942.02, 1992.02};
  //double target_z[9]={0, 50, 100, 938.03, 988.03, 1038.03, 1892.02, 1942.02, 1992.02};
  TString name="final";

// plot resolution
#if 0
  TFile *f1 = new TFile("output/8plane_muMinus/resolution.root");
  TFile *f2 = new TFile("output/9plane_muMinus/resolution.root");
  TH1D *h1_p_perp = (TH1D*)f1->Get("p_perp_reso");
  TH1D *h1_x0 = (TH1D*)f1->Get("x0_reso");
  TH1D *h1_y0 = (TH1D*)f1->Get("y0_reso");
  TH1D *h1_phi0 = (TH1D*)f1->Get("phi0_reso");
  TH1D *h1_lambda = (TH1D*)f1->Get("lambda_reso");
  TH1D *h2_p_perp = (TH1D*)f2->Get("p_perp_reso");
  TH1D *h2_x0 = (TH1D*)f2->Get("x0_reso");
  TH1D *h2_y0 = (TH1D*)f2->Get("y0_reso");
  TH1D *h2_phi0 = (TH1D*)f2->Get("phi0_reso");
  TH1D *h2_lambda = (TH1D*)f2->Get("lambda_reso");

  TCanvas *c1 = new TCanvas("c1","",800,600);
  c1->cd();


  double ymax=0, ymin=0;
  ymax = h1_p_perp->GetMaximum()>h2_p_perp->GetMaximum()?h1_p_perp->GetMaximum():h2_p_perp->GetMaximum();
  ymin = h1_p_perp->GetMinimum()>h2_p_perp->GetMinimum()?h1_p_perp->GetMinimum():h2_p_perp->GetMinimum();
  h1_p_perp->GetYaxis()->SetRangeUser(0, 1.5*ymax);
  h1_p_perp->GetXaxis()->SetTitle("p_{T, truth} [GeV]");
  h1_p_perp->GetYaxis()->SetTitle("#sigma(#frac{p_{T, reco}-p_{T, truth}}{p_{T, truth}})");
  h1_p_perp->Draw("L,P,E1");
  h2_p_perp->SetLineColor(kRed);
  h2_p_perp->SetMarkerColor(kRed);
  h2_p_perp->Draw("L,P,E1,same");
  TLegend *leg = new TLegend(0.6, 0.75, 0.9, 0.9);
   leg -> AddEntry(h1_p_perp,"2-4-2 layout", "LPE");
   leg -> AddEntry(h2_p_perp,"3-3-3 layout", "LPE");
   leg -> SetFillStyle(0);
   leg -> SetBorderSize(0);
   leg -> SetTextSize(0.04);
  leg->Draw();
  ATLASLabel(0.2,0.85,"Internal");
  c1->Update();
  c1->SaveAs(Form("plots/%s/p_perp_resolution_profile.png",name.Data()));
  ymax = h1_x0->GetMaximum()>h2_x0->GetMaximum()?h1_x0->GetMaximum():h2_x0->GetMaximum();
  ymin = h1_x0->GetMinimum()>h2_x0->GetMinimum()?h1_x0->GetMinimum():h2_x0->GetMinimum();
  h1_x0->GetYaxis()->SetRangeUser(0, 1.5*ymax);
  h1_x0->GetXaxis()->SetTitle("p_{T, truth} [GeV]");
  h1_x0->GetYaxis()->SetTitle("#sigma(x_{0}) [mm]");
  h1_x0->Draw("L,P,E1");
  h2_x0->SetLineColor(kRed);
  h2_x0->SetMarkerColor(kRed);
  h2_x0->Draw("L,P,E1,same");
  leg->Draw();
  ATLASLabel(0.2,0.85,"Internal");
  c1->Update();
  c1->SaveAs(Form("plots/%s/x0_resolution_profile.png",name.Data()));
  ymax = h1_y0->GetMaximum()>h2_y0->GetMaximum()?h1_y0->GetMaximum():h2_y0->GetMaximum();
  ymin = h1_y0->GetMinimum()>h2_y0->GetMinimum()?h1_y0->GetMinimum():h2_y0->GetMinimum();
  h1_y0->GetYaxis()->SetRangeUser(0, 1.5*ymax);
  h1_y0->GetXaxis()->SetTitle("p_{T, truth} [GeV]");
  h1_y0->GetYaxis()->SetTitle("#sigma(y_{0}) [mm]");
  h1_y0->Draw("L,P,E1");
  h2_y0->SetLineColor(kRed);
  h2_y0->SetMarkerColor(kRed);
  h2_y0->Draw("L,P,E1,same");
  leg->Draw();
  ATLASLabel(0.2,0.85,"Internal");
  c1->Update();
  c1->SaveAs(Form("plots/%s/y0_resolution_profile.png",name.Data()));
  ymax = h1_phi0->GetMaximum()>h2_phi0->GetMaximum()?h1_phi0->GetMaximum():h2_phi0->GetMaximum();
  ymin = h1_phi0->GetMinimum()>h2_phi0->GetMinimum()?h1_phi0->GetMinimum():h2_phi0->GetMinimum();
  h1_phi0->GetYaxis()->SetRangeUser(0, 1.5*ymax);
  h1_phi0->GetXaxis()->SetTitle("p_{T, truth} [GeV]");
  h1_phi0->GetYaxis()->SetTitle("#sigma(#phi_{0}) [rad]");
  h1_phi0->Draw("L,P,E1");
  h2_phi0->SetLineColor(kRed);
  h2_phi0->SetMarkerColor(kRed);
  h2_phi0->Draw("L,P,E1,same");
  leg->Draw();
  ATLASLabel(0.2,0.85,"Internal");
  c1->Update();
  c1->SaveAs(Form("plots/%s/phi0_resolution_profile.png",name.Data()));
  ymax = h1_lambda->GetMaximum()>h2_lambda->GetMaximum()?h1_lambda->GetMaximum():h2_lambda->GetMaximum();
  ymin = h1_lambda->GetMinimum()>h2_lambda->GetMinimum()?h1_lambda->GetMinimum():h2_lambda->GetMinimum();
  h1_lambda->GetYaxis()->SetRangeUser(0, 1.5*ymax);
  h1_lambda->GetXaxis()->SetTitle("p_{T, truth} [GeV]");
  h1_lambda->GetYaxis()->SetTitle("#sigma(#lambda) [rad]");
  h1_lambda->Draw("L,P,E1");
  h2_lambda->SetLineColor(kRed);
  h2_lambda->SetMarkerColor(kRed);
  h2_lambda->Draw("L,P,E1,same");
  leg->Draw();
  ATLASLabel(0.2,0.85,"Internal");
  c1->Update();
  c1->SaveAs(Form("plots/%s/lambda_resolution_profile.png",name.Data()));
#endif

// plot mean
#if 1
  TFile *f1 = new TFile("output/8plane_muMinus/resolution.root");
  TFile *f2 = new TFile("output/9plane_muMinus/resolution.root");
  TH1D *h1_p_perp = (TH1D*)f1->Get("p_perp_mean");
  TH1D *h1_x0 = (TH1D*)f1->Get("x0_mean");
  TH1D *h1_y0 = (TH1D*)f1->Get("y0_mean");
  TH1D *h1_phi0 = (TH1D*)f1->Get("phi0_mean");
  TH1D *h1_lambda = (TH1D*)f1->Get("lambda_mean");
  TH1D *h2_p_perp = (TH1D*)f2->Get("p_perp_mean");
  TH1D *h2_x0 = (TH1D*)f2->Get("x0_mean");
  TH1D *h2_y0 = (TH1D*)f2->Get("y0_mean");
  TH1D *h2_phi0 = (TH1D*)f2->Get("phi0_mean");
  TH1D *h2_lambda = (TH1D*)f2->Get("lambda_mean");

  TCanvas *c1 = new TCanvas("c1","",800,600);
  c1->cd();


  double ymax=0, ymin=0;
  ymax = h1_p_perp->GetMaximum()>h2_p_perp->GetMaximum()?h1_p_perp->GetMaximum():h2_p_perp->GetMaximum();
  ymin = h1_p_perp->GetMinimum()>h2_p_perp->GetMinimum()?h1_p_perp->GetMinimum():h2_p_perp->GetMinimum();
  h1_p_perp->GetYaxis()->SetRangeUser(ymin-fabs(0.5*ymax), 1.5*ymax);
  h1_p_perp->GetXaxis()->SetTitle("p_{T, truth} [GeV]");
  h1_p_perp->GetYaxis()->SetTitle("#frac{p_{T, reco} - p_{T, truth}}{p_{T, truth}}");
  h1_p_perp->Draw("L,P,E1");
  h2_p_perp->SetLineColor(kRed);
  h2_p_perp->SetMarkerColor(kRed);
  h2_p_perp->Draw("L,P,E1,same");
  TLegend *leg = new TLegend(0.6, 0.75, 0.9, 0.9);
   leg -> AddEntry(h1_p_perp,"2-4-2 layout", "LPE");
   leg -> AddEntry(h2_p_perp,"3-3-3 layout", "LPE");
   leg -> SetFillStyle(0);
   leg -> SetBorderSize(0);
   leg -> SetTextSize(0.04);
  leg->Draw();
  ATLASLabel(0.2,0.85,"Internal");
  c1->Update();
  c1->SaveAs(Form("plots/%s/p_perp_mean_profile.png",name.Data()));
  ymax = h1_x0->GetMaximum()>h2_x0->GetMaximum()?h1_x0->GetMaximum():h2_x0->GetMaximum();
  ymin = h1_x0->GetMinimum()>h2_x0->GetMinimum()?h1_x0->GetMinimum():h2_x0->GetMinimum();
  h1_x0->GetYaxis()->SetRangeUser(0, 1.5*ymax);
  h1_x0->GetYaxis()->SetRangeUser(ymin-fabs(0.5*ymax), 1.5*ymax);
  h1_x0->GetXaxis()->SetTitle("p_{T, truth} [GeV]");
  h1_x0->GetYaxis()->SetTitle("X_{0, reco}-X_{0, truth} [mm]");
  h1_x0->Draw("L,P,E1");
  h2_x0->SetLineColor(kRed);
  h2_x0->SetMarkerColor(kRed);
  h2_x0->Draw("L,P,E1,same");
  leg->Draw();
  ATLASLabel(0.2,0.85,"Internal");
  c1->Update();
  c1->SaveAs(Form("plots/%s/x0_mean_profile.png",name.Data()));
  ymax = h1_y0->GetMaximum()>h2_y0->GetMaximum()?h1_y0->GetMaximum():h2_y0->GetMaximum();
  ymin = h1_y0->GetMinimum()>h2_y0->GetMinimum()?h1_y0->GetMinimum():h2_y0->GetMinimum();
  h1_y0->GetYaxis()->SetRangeUser(0, 1.5*ymax);
  h1_y0->GetYaxis()->SetRangeUser(ymin-fabs(0.5*ymax), 1.5*ymax);
  h1_y0->GetXaxis()->SetTitle("p_{T, truth} [GeV]");
  h1_y0->GetYaxis()->SetTitle("Y_{0, reco}-Y_{0, truth} [mm]");
  h1_y0->Draw("L,P,E1");
  h2_y0->SetLineColor(kRed);
  h2_y0->SetMarkerColor(kRed);
  h2_y0->Draw("L,P,E1,same");
  leg->Draw();
  ATLASLabel(0.2,0.85,"Internal");
  c1->Update();
  c1->SaveAs(Form("plots/%s/y0_mean_profile.png",name.Data()));
  ymax = h1_phi0->GetMaximum()>h2_phi0->GetMaximum()?h1_phi0->GetMaximum():h2_phi0->GetMaximum();
  ymin = h1_phi0->GetMinimum()>h2_phi0->GetMinimum()?h1_phi0->GetMinimum():h2_phi0->GetMinimum();
  h1_phi0->GetYaxis()->SetRangeUser(0, 1.5*ymax);
  h1_phi0->GetYaxis()->SetRangeUser(ymin-fabs(0.5*ymax), 1.5*ymax);
  h1_phi0->GetXaxis()->SetTitle("p_{T, truth} [GeV]");
  h1_phi0->GetYaxis()->SetTitle("#phi_{0, reco}-#phi_{0, truth} [rad]");
  h1_phi0->Draw("L,P,E1");
  h2_phi0->SetLineColor(kRed);
  h2_phi0->SetMarkerColor(kRed);
  h2_phi0->Draw("L,P,E1,same");
  leg->Draw();
  ATLASLabel(0.2,0.85,"Internal");
  c1->Update();
  c1->SaveAs(Form("plots/%s/phi0_mean_profile.png",name.Data()));
  ymax = h1_lambda->GetMaximum()>h2_lambda->GetMaximum()?h1_lambda->GetMaximum():h2_lambda->GetMaximum();
  ymin = h1_lambda->GetMinimum()>h2_lambda->GetMinimum()?h1_lambda->GetMinimum():h2_lambda->GetMinimum();
  h1_lambda->GetYaxis()->SetRangeUser(ymin-fabs(0.5*ymax), 1.5*ymax);
  h1_lambda->GetXaxis()->SetTitle("p_{T, truth} [GeV]");
  h1_lambda->GetYaxis()->SetTitle("#lambda_{reco}-#lambda_{truth} [rad]");
  h1_lambda->Draw("L,P,E1");
  h2_lambda->SetLineColor(kRed);
  h2_lambda->SetMarkerColor(kRed);
  h2_lambda->Draw("L,P,E1,same");
  leg->Draw();
  ATLASLabel(0.2,0.85,"Internal");
  c1->Update();
  c1->SaveAs(Form("plots/%s/lambda_mean_profile.png",name.Data()));
#endif
// plot efficiency
#if 0
  TFile *f1 = new TFile("output/8plane_muMinus/efficiency.root");
  TFile *f2 = new TFile("output/9plane_muMinus/efficiency.root");
  TH1D *h1_pper = (TH1D*)f1->Get("pper_eff");
  TH1D *h1_x0 = (TH1D*)f1->Get("x0_eff");
  TH1D *h1_y0 = (TH1D*)f1->Get("y0_eff");
  TH1D *h1_phi0 = (TH1D*)f1->Get("phi0_eff");
  TH1D *h1_lambda = (TH1D*)f2->Get("lambda_eff");
  TH1D *h2_pper = (TH1D*)f2->Get("pper_eff");
  TH1D *h2_x0 = (TH1D*)f2->Get("x0_eff");
  TH1D *h2_y0 = (TH1D*)f2->Get("y0_eff");
  TH1D *h2_phi0 = (TH1D*)f2->Get("phi0_eff");
  TH1D *h2_lambda = (TH1D*)f2->Get("lambda_eff");

  TCanvas *c1 = new TCanvas("c1","",800,600);
  c1->cd();
  h1_pper->Draw();
  h2_pper->SetLineColor(kRed);
  h2_pper->SetMarkerColor(kRed);
  h2_pper->Draw("same");
  TLegend *leg = new TLegend(0.6, 0.75, 0.9, 0.9);
   leg -> AddEntry(h1_pper,"2-4-2 layout", "LPE");
   leg -> AddEntry(h2_pper,"3-3-3 layout", "LPE");
   leg -> SetFillStyle(0);
   leg -> SetBorderSize(0);
   leg -> SetTextSize(0.04);
  leg->Draw();
  ATLASLabel(0.2,0.85,"Internal");
  c1->Update();
  c1->SaveAs(Form("plots/%s/pper_efficiency.png",name.Data()));

  h1_x0->Draw();
  h2_x0->SetLineColor(kRed);
  h2_x0->SetMarkerColor(kRed);
  h2_x0->Draw("same");
  leg->Draw();
  ATLASLabel(0.2,0.85,"Internal");
  c1->Update();
  c1->SaveAs(Form("plots/%s/x0_efficiency.png",name.Data()));

  h1_y0->Draw();
  h2_y0->SetLineColor(kRed);
  h2_y0->SetMarkerColor(kRed);
  h2_y0->Draw("same");
  leg->Draw();
  ATLASLabel(0.2,0.85,"Internal");
  c1->Update();
  c1->SaveAs(Form("plots/%s/y0_efficiency.png",name.Data()));

  h1_phi0->Draw();
  h2_phi0->SetLineColor(kRed);
  h2_phi0->SetMarkerColor(kRed);
  h2_phi0->Draw("same");
  leg->Draw();
  ATLASLabel(0.2,0.85,"Internal");
  c1->Update();
  c1->SaveAs(Form("plots/%s/phi0_efficiency.png",name.Data()));

  h1_lambda->Draw();
  h2_lambda->SetLineColor(kRed);
  h2_lambda->SetMarkerColor(kRed);
  h2_lambda->Draw("same");
  leg->Draw();
  ATLASLabel(0.2,0.85,"Internal");
  c1->Update();
  c1->SaveAs(Form("plots/%s/lambda_efficiency.png",name.Data()));

#endif
}
