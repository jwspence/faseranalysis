#include "TTree.h"
#include "TProfile.h"
#include "AtlasLabels.h"
#include "AtlasLabels.C"
#include "AtlasStyle.h"
#include "AtlasStyle.C"
#include "AtlasUtils.h"
#include "AtlasUtils.C"
#include <vector>

double getFitRange(TH1 * h, double median, double heightFraction)
{
  double height = h->GetBinContent(h->FindBin(median));
  cout << "Get bin content" << endl;
  double threshold = heightFraction * height;
  int binHigh = 0;
  for (int i=h->FindBin(median); i < h->GetNbinsX(); ++i) {
    if (h->GetBinContent(i) < threshold && binHigh==0) {
      binHigh = i;
      break;
    }
  }
  int binLow = 0;
  for(int i=h->FindBin(median); i > 0; --i){
    if (h->GetBinContent(i) < threshold && binLow==0) {
      binLow = i;
      break;
    }
  }
  double sigmaHigh = h->GetBinCenter(binHigh)-h->GetBinCenter(h->FindBin(median));
  double sigmaLow  = h->GetBinCenter(binLow)-h->GetBinCenter(h->FindBin(median));
  double sigma = 0.5*(sigmaHigh + sigmaLow);
  return sigma;
}


vector<TH1D*> makeResolutionPlot(TH2D * h2D, TString fname) 
{
  TH1D* h1D = new TH1D("res", "", 15, 0, 3000);
  TH1D* h1D_2 = new TH1D("mean", "", 15, 0, 3000);

  TCanvas c5;
  c5.SetBatch();
  c5.cd();
  TFile check(Form("output/check_%s.root",fname.Data()), "recreate");
  for (int i=1; i<=h2D->GetNbinsX(); ++i) {
    string hname = Form("Res_bin_%i",i);
    TH1D* h = h2D->ProjectionY(hname.c_str(),i,i+1);
    double center = h->GetBinCenter(h->GetMaximumBin());
    double fitRange = 2*h->GetRMS();
    double rMax = center + fitRange;
    double rMin = center - fitRange;
    TF1 * fGauss = new TF1("fGauss", "gaus", rMax, rMin);
    h->Fit("fGauss", "RQL");//use likelihood
    double sigma = fGauss->GetParameter(2);
    double sigmaError = fGauss->GetParError(2);
    fitRange = 2*sigma;
    rMax = fGauss->GetParameter(1)+fitRange;
    rMin = fGauss->GetParameter(1) - fitRange;
    TF1 * fGauss_1 = new TF1("fGauss_1", "gaus", rMax, rMin);
    h->Fit("fGauss_1","RGL");
    sigma = fGauss_1->GetParameter(2);
    sigmaError = fGauss_1->GetParError(2);
    double mean = fGauss_1->GetParameter(1);
    double meanError = fGauss_1->GetParError(1);

    h1D->SetBinContent(i, sigma); // sigma
    h1D->SetBinError(i, sigmaError); // sigma
    h1D_2->SetBinContent(i, mean);
    h1D_2->SetBinError(i, meanError);
    /*
    if (TMath::Abs(sigmaError) < 0.5*TMath::Abs(sigma)) {
      h1D->SetBinError(i, sigmaError); // error on sigma
    }
    */

    check.cd();
    h->Write();
  }
  vector<TH1D*> meanAndSigma;
  meanAndSigma.push_back(h1D_2);
  meanAndSigma.push_back(h1D);
  return meanAndSigma;
}
//////////////////////////////////////////////////////////////
void plot_TP()
{
  SetAtlasStyle();

  TString name="TP_muMinus_0p6T";
//cout << "Setting n" << endl;
  TFile *f = new TFile("helix_tracker_TP_output_muMinus_0p6T.root");
  TTree *t = (TTree*)f->Get("tracks");
cout << "Got tracks tree from input file" << endl;
  double truth_px, truth_py, truth_pz, truth_R, truth_phi0, truth_lambda, reco_px, reco_py, reco_pz, reco_R, reco_phi0, reco_lambda;
  double chi_square;
  int pdgID, N_track_points;
    std::vector<double> *space_points_x=0;
    std::vector<double> *space_points_y=0;
    std::vector<double> *space_points_z=0;
    std::vector<double> *track_points_x=0;
    std::vector<double> *track_points_y=0;
    std::vector<double> *track_points_z=0;
    std::vector<int> *track_points_rowID=0;
    std::vector<double> *truth_x=0;
    std::vector<double> *truth_y=0;
    std::vector<double> *truth_z=0;
    std::vector<double> *reco_x=0;
    std::vector<double> *reco_y=0;
    std::vector<double> *reco_z=0;
cout << "Declared variables" << endl;
  t->SetBranchAddress("chi_square", &chi_square);
  t->SetBranchAddress("pdgID", &pdgID);
  t->SetBranchAddress("N_track_points", &N_track_points);
  t->SetBranchAddress("truth_px", &truth_px);
  t->SetBranchAddress("truth_py", &truth_py);
  t->SetBranchAddress("truth_pz", &truth_pz);
  t->SetBranchAddress("truth_R", &truth_R);
  t->SetBranchAddress("truth_phi0", &truth_phi0);
  t->SetBranchAddress("truth_lambda", &truth_lambda);
  t->SetBranchAddress("reco_px", &reco_px);
  t->SetBranchAddress("reco_py", &reco_py);
  t->SetBranchAddress("reco_pz", &reco_pz);
  t->SetBranchAddress("reco_R", &reco_R);
  t->SetBranchAddress("reco_phi0", &reco_phi0);
  t->SetBranchAddress("reco_lambda", &reco_lambda);
  t->SetBranchAddress("truth_x", &truth_x);
  t->SetBranchAddress("truth_y", &truth_y);
  t->SetBranchAddress("truth_z", &truth_z);
  t->SetBranchAddress("reco_x", &reco_x);
  t->SetBranchAddress("reco_y", &reco_y);
  t->SetBranchAddress("reco_z", &reco_z);
  t->SetBranchAddress("space_points_x", &space_points_x);
  t->SetBranchAddress("space_points_y", &space_points_y);
  t->SetBranchAddress("space_points_z", &space_points_z);
  t->SetBranchAddress("track_points_x", &track_points_x);
  t->SetBranchAddress("track_points_y", &track_points_y);
  t->SetBranchAddress("track_points_z", &track_points_z);
  t->SetBranchAddress("track_points_rowID", &track_points_rowID);
cout << "Set addresses for each branch" << endl;
// for Gaussian fit
#if 1
  TFile *f_out = new TFile(Form("output/%s/TP_resolution_0p6T.root",name.Data()),"recreate");
cout << "Set output file" << endl;
  TH2D *h_p = new TH2D("p","",15,0,3000,1000,-20,20);
  TH2D *h_p_perp = new TH2D("p_perp","",15,0,3000,1000,-20,20);
  TH2D *h_x0 = new TH2D("x0","",10,0,500,500,-10,10);
  TH2D *h_y0 = new TH2D("y0","",10,0,500,100,-10,10);
  TH2D *h_phi0 = new TH2D("phi0","",10,0,500,600,-0.03,0.03);
  TH2D *h_lambda = new TH2D("lambda","",10,0,500,600,-0.03,0.03);
cout << "Created histograms" << endl;
#endif

  double truth_pperp, truth_p, reco_pperp, reco_p;
  for (int iEntry=0; iEntry < t->GetEntries(); iEntry++)
    {
      t->GetEntry(iEntry);
      if (chi_square/(N_track_points*2-5)<1000) 
      {
      truth_pperp = sqrt(truth_px*truth_px + truth_pz*truth_pz);
      reco_pperp = sqrt(reco_px*reco_px + reco_pz*reco_pz);
      truth_p = sqrt(truth_px*truth_px + truth_py*truth_py + truth_pz*truth_pz);
      reco_p = sqrt(reco_px*reco_px + reco_py*reco_py + reco_pz*reco_pz);
cout << "defined truth_p = " << truth_p << endl;
cout << "defined reco_p = " << reco_p << endl;
      h_p->Fill(truth_p, (reco_p-truth_p)/truth_p);
cout << "filled histogram h_p with value " << (reco_p-truth_p)/truth_p << endl;
      h_p_perp->Fill(truth_pperp, (reco_pperp-truth_pperp)/truth_pperp);
      h_x0->Fill(truth_pperp, reco_x->at(0) - truth_x->at(0));
      h_y0->Fill(truth_pperp, reco_y->at(0) - truth_y->at(0));
      h_phi0->Fill(truth_pperp, reco_phi0-truth_phi0);
      h_lambda->Fill(truth_pperp, reco_lambda-truth_lambda);
      }
    }
  vector<TH1D*> p_meanAndSigma = makeResolutionPlot(h_p,Form("p_%s",name.Data()));
cout << "Made resolution plot" << endl;
  TH1D* h_p_Mean = p_meanAndSigma[0];
  TH1D* h_p_Sigma = p_meanAndSigma[1];
  h_p_Mean->SetName("p_mean");
  h_p_Sigma->SetName("p_reso");
cout << "Set name of h_p_Sigma" << endl;
  vector<TH1D*> p_perp_meanAndSigma = makeResolutionPlot(h_p_perp,Form("p_perp_%s",name.Data()));
  TH1D* h_p_perp_Mean = p_perp_meanAndSigma[0];
  TH1D* h_p_perp_Sigma = p_perp_meanAndSigma[1];
  h_p_perp_Mean->SetName("p_perp_mean");
  h_p_perp_Sigma->SetName("p_perp_reso");
  vector<TH1D*> x0_meanAndSigma = makeResolutionPlot(h_x0,Form("x0_%s",name.Data()));
  TH1D* h_x0_Mean = x0_meanAndSigma[0];
  TH1D* h_x0_Sigma = x0_meanAndSigma[1];
  h_x0_Mean->SetName("x0_mean");
  h_x0_Sigma->SetName("x0_reso");
  vector<TH1D*> y0_meanAndSigma = makeResolutionPlot(h_y0,Form("y0_%s",name.Data()));
  TH1D* h_y0_Mean = y0_meanAndSigma[0];
  TH1D* h_y0_Sigma = y0_meanAndSigma[1];
  h_y0_Mean->SetName("y0_mean");
  h_y0_Sigma->SetName("y0_reso");
  vector<TH1D*> phi0_meanAndSigma = makeResolutionPlot(h_phi0,Form("phi0_%s",name.Data()));
  TH1D* h_phi0_Mean = phi0_meanAndSigma[0];
  TH1D* h_phi0_Sigma = phi0_meanAndSigma[1];
  h_phi0_Mean->SetName("phi0_mean");
  h_phi0_Sigma->SetName("phi0_reso");
  vector<TH1D*> lambda_meanAndSigma = makeResolutionPlot(h_lambda,Form("lambda_%s",name.Data()));
  TH1D* h_lambda_Mean = lambda_meanAndSigma[0];
  TH1D* h_lambda_Sigma = lambda_meanAndSigma[1];
  h_lambda_Mean->SetName("lambda_mean");
  h_lambda_Sigma->SetName("lambda_reso");
  


  TH1D *karimaki = new TH1D("karimaki","",10,0,2000);
//  0.5T B field  
#if 0
  karimaki->SetBinContent(1, 0.023);
  karimaki->SetBinContent(2, 0.035);
  karimaki->SetBinContent(3, 0.051);
  karimaki->SetBinContent(4, 0.068);
  karimaki->SetBinContent(5, 0.087);
  karimaki->SetBinContent(6, 0.107);
  karimaki->SetBinContent(7, 0.123);
  karimaki->SetBinContent(8, 0.141);
  karimaki->SetBinContent(9, 0.160);
  karimaki->SetBinContent(10,0.178);
#endif
//  0.6T B field  
#if 1
  karimaki->SetBinContent(1, 0.01950625098);
  karimaki->SetBinContent(2, 0.02940143609);
  karimaki->SetBinContent(3, 0.04280590706);
  karimaki->SetBinContent(4, 0.05730791857);
  karimaki->SetBinContent(5, 0.07224956747);
  karimaki->SetBinContent(6, 0.08740568109);
  karimaki->SetBinContent(7, 0.1026813361);
  karimaki->SetBinContent(8, 0.118030128);
  karimaki->SetBinContent(9, 0.1334268191);
  karimaki->SetBinContent(10,0.1488565471);
#endif
  karimaki->SetBinError(1,0.00001);
  karimaki->SetBinError(2,0.00001);
  karimaki->SetBinError(3,0.00001);
  karimaki->SetBinError(4,0.00001);
  karimaki->SetBinError(5,0.00001);
  karimaki->SetBinError(6,0.00001);
  karimaki->SetBinError(7,0.00001);
  karimaki->SetBinError(8,0.00001);
  karimaki->SetBinError(9,0.00001);
  karimaki->SetBinError(10,0.00001);
  TCanvas *c1 = new TCanvas("c1","",800,600);
  c1->cd();
  karimaki->SetLineColor(kRed);
  karimaki->SetMarkerColor(kRed);
  karimaki->SetMarkerStyle(21);
  karimaki->GetXaxis()->SetTitle("p(#mu) [GeV]");
  karimaki->GetYaxis()->SetTitle("#frac{#sigma(p)}{p}");
  karimaki->GetYaxis()->SetRangeUser(0,0.4);
//  karimaki->Draw("P,E1");
//  h_p_Sigma->Draw("P,E1,same");
  h_p_Sigma->Draw("P,E1");
cout << "Drew simulated resolution" << endl;
  TLegend *leg = new TLegend(0.6, 0.75, 0.9, 0.9);
   leg -> AddEntry(karimaki,"Predicted", "LPE");
   leg -> AddEntry(h_p_Sigma,"Simulation", "LPE");
   leg -> SetFillStyle(0);
   leg -> SetBorderSize(0);
   leg -> SetTextSize(0.04);
  leg->Draw();
  ATLASLabel(0.2,0.85,"Preliminary");
  c1->Update();
  c1->SaveAs(Form("plots/%s/p_resolution_0p6T.pdf",name.Data()));
  
  f_out->cd();
  h_p->Write();
  h_p_perp->Write();
  h_x0->Write();
  h_y0->Write();
  h_phi0->Write();
  h_lambda->Write();
  h_p_Mean->Write();
  h_p_perp_Mean->Write();
  h_x0_Mean->Write();
  h_y0_Mean->Write();
  h_phi0_Mean->Write();
  h_lambda_Mean->Write();
  h_p_Sigma->Write();
  h_p_perp_Sigma->Write();
  h_x0_Sigma->Write();
  h_y0_Sigma->Write();
  h_phi0_Sigma->Write();
  h_lambda_Sigma->Write();
  f_out->Close();
}
