#include "TTree.h"
#include "TProfile.h"
#include "AtlasLabels.h"
#include "AtlasLabels.C"
#include "AtlasStyle.h"
#include "AtlasStyle.C"
#include "AtlasUtils.h"
#include "AtlasUtils.C"
#include <string>

double getMedian(TH1 * h) {
  double x, q;
  q = 0.5; // 0.5 for "median"
  if (h->GetEntries()==0) return 0; // to avoid warning
  h->ComputeIntegral(); // additional precaution
  h->GetQuantiles(1, &x, &q);
  return x;
}


double getFitRange(TH1 * h, double median, double heightFraction)
{
  double height = h->GetBinContent(h->FindBin(median));
  double threshold = heightFraction * height;
  int binHigh = 0;
  for (int i=h->FindBin(median); i < h->GetNbinsX(); ++i) {
    if (h->GetBinContent(i) < threshold && binHigh==0) {
      binHigh = i;
      break;
    }
  }
  int binLow = 0;
  for(int i=h->FindBin(median); i > 0; --i){
    if (h->GetBinContent(i) < threshold && binLow==0) {
      binLow = i;
      break;
    }
  }
  double sigmaHigh = h->GetBinCenter(binHigh)-h->GetBinCenter(h->FindBin(median));
  double sigmaLow  = h->GetBinCenter(binLow)-h->GetBinCenter(h->FindBin(median));
  double sigma = 0.5*(sigmaHigh + sigmaLow);
  return sigma;
}


TH1D * makeResolutionPlot(TH2D * h2D, const string & axis) {
  //TGraphErrors * makeResolutionPlot(TH2D * h2D, const string & axis) {
  cout << "Making basic resolution plots\n";

  TH1D* h1D = new TH1D("res", "res", 144, -0.5, 143.5);
  //TGraphErrors * h1D = new TGraphErrors;
  h1D->GetXaxis()->SetTitle("rowID");
  h1D->GetYaxis()->SetTitle(("#sigma_{"+axis+"_{space point}-"+axis+"_{truth hit}}").c_str());

  TCanvas c5;
  c5.SetBatch();
  c5.cd();
  TFile check("check.root", "recreate");
  for (int i=1; i<h2D->GetNbinsX(); ++i) {
    string hname = Form("Res_bin_%i",i);
    TH1D* h = h2D->ProjectionY(hname.c_str(),i,i+1);
    //Fit core to Gaussian
    //double nFitSigma=nSigma;
    //double median = getMedian(h);
    //double delta = fabs(h->GetMean()-median)/fabs(h->GetMean())*100.;
    //double FWHM = getFitRange(h, median, 0.3);
    double center = h->GetBinCenter(h->GetMaximumBin());
    //double fitRange = 3.0*getFitRange(h, center, 0.3);
    double fitRange = 2*h->GetRMS();

    //double mean = h->GetMean();
    //double nFitSigma = 2.;
    //double rMax = mean + nFitSigma * h->GetStdDev();
    //double rMin = mean - nFitSigma * h->GetStdDev();
    double rMax = center + fitRange;
    double rMin = center - fitRange;
    TF1 * fGauss = new TF1("fGauss", "gaus", rMax, rMin);

    //fGauss->SetParameter(1, h->GetMean());
    //fGauss->SetParameter(2, h->GetStdDev());
    //fGauss->SetParameter(1, median);
    //fGauss->SetParameter(2, FWHM*1.2);
    //fGauss->SetParameter(2, h->GetStdDev());
    h->Fit("fGauss", "RQL");//use likelihood
    //h->Write();
    double rowID = i-1;
    double sigma = fGauss->GetParameter(2);
    double sigmaError = fGauss->GetParError(2);
    fitRange = 2*sigma;
    rMax = fGauss->GetParameter(1)+fitRange;
    rMin = fGauss->GetParameter(1) - fitRange;
    TF1 * fGauss_1 = new TF1("fGauss_1", "gaus", rMax, rMin);
    h->Fit("fGauss_1","RGL");
    sigma = fGauss_1->GetParameter(2);


    h1D->SetBinContent(i, sigma); // sigma
    if (TMath::Abs(sigmaError) < 0.5*TMath::Abs(sigma)) {
      h1D->SetBinError(i, sigmaError); // error on sigma
    }
    //size_t nPoint = h1D->GetN();
    //h1D->SetPoint(nPoint, rowID, mean*fGauss->GetParameter(2)); // sigma
    //h1D->SetPointError(nPoint, rowID, mean*fGauss->GetParError(2)); // error on sigma
    //cout << "DEBUG  Set rowID " << rowID << " sigma = " << fGauss->GetParameter(2) << ", sigma error = " << fGauss->GetParError(2) << '\n';
    check.cd();
    h->Write();
  }

  return h1D;
}

void plot_profile()
{
  SetAtlasStyle();
  const int N_plane=9;
  TString name="9plane_muMinus";

  TFile *f = new TFile("out_muMinus.root");
  //TFile *f = new TFile("out_muPlus.root");
  TH2D *h_x = (TH2D*)f->Get("hist_x");
  TH2D *h_y = (TH2D*)f->Get("hist_y");

  //TH1D* hh=makeResolutionPlot(h_x,"X");
  TH1D* hh=makeResolutionPlot(h_x,"X");
  hh->Draw();

  /*
  TProfile *p_x = h_x->ProfileX("x_profile");
  TProfile *p_y = h_y->ProfileX("y_profile");
  TH1D *h_x_RMS = new TH1D("x_RMS","",16*N_plane,0,16*N_plane);
  TH1D *h_y_RMS = new TH1D("y_RMS","",16*N_plane,0,16*N_plane);
  for (int i=1; i<=16*N_plane; i++)
    {
      h_x_RMS->SetBinContent(i,p_x->GetBinError(i));
      h_y_RMS->SetBinContent(i,p_y->GetBinError(i));
    }
  TH1D *hh=h_x->ProjectionY("hh",1,2);
  hh->Draw();
cout<<hh->GetRMS();
  TCanvas *c1 = new TCanvas("c1","",800,600);
  c1->cd();
  p_x->GetXaxis()->SetTitle("rowID");
  p_x->GetYaxis()->SetTitle("x_{Space Point}-x_{Truth Hits} [mm]");
  p_x->SetMarkerSize(0.5);
  p_x->Draw();
  c1->Update();
  c1->SaveAs(Form("plots/%s/Aaron_truth_residual_X_profile.png",name.Data()));
  p_y->GetXaxis()->SetTitle("rowID");
  p_y->GetYaxis()->SetTitle("y_{Space Point}-y_{Truth Hits} [mm]");
  p_y->SetMarkerSize(0.5);
  p_y->Draw();
  c1->Update();
  c1->SaveAs(Form("plots/%s/Aaron_truth_residual_Y_profile.png",name.Data()));
  h_x_RMS->GetXaxis()->SetTitle("rowID");
  h_x_RMS->GetYaxis()->SetTitle("RMS(x_{Space Point}-x_{Truth Hits}) [mm]");
  h_x_RMS->SetMarkerSize(0.5);
  h_x_RMS->Draw("P");
  c1->Update();
  c1->SaveAs(Form("plots/%s/Aaron_truth_residual_X_RMS.png",name.Data()));
  h_y_RMS->GetXaxis()->SetTitle("rowID");
  h_y_RMS->GetYaxis()->SetTitle("RMS(y_{Space Point}-y_{Truth Hits}) [mm]");
  h_y_RMS->SetMarkerSize(0.5);
  h_y_RMS->Draw("P");
  c1->Update();
  c1->SaveAs(Form("plots/%s/Aaron_truth_residual_Y_RMS.png",name.Data()));
  */
}
