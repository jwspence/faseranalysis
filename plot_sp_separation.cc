#include "TTree.h"
#include "TProfile.h"
#include "AtlasLabels.h"
#include "AtlasLabels.C"
#include "AtlasStyle.h"
#include "AtlasStyle.C"
#include "AtlasUtils.h"
#include "AtlasUtils.C"

void plot_sp_separation()
{
  TString name="9plane_muMinus";
  const int N_plane=9;
  //double target_z[8]={0, 50, 909.04, 959.04, 1009.04, 1059.04, 1942.02, 1992.02};
  //double target_z[9]={0, 50, 100, 938.03, 988.03, 1038.03, 1892.02, 1942.02, 1992.02};

  TFile *f = new TFile("sp_separation_output.root");
  TTree *t = (TTree*)f->Get("sp_separation");
  int N_shared_sp,status;
    std::vector<double> *truth_px;
    std::vector<double> *truth_py;
    std::vector<double> *truth_pz;
    std::vector<double> *d_truth_hits;
    std::vector<int> *Is_shared_sp;
  t->SetBranchAddress("N_shared_sp", &N_shared_sp);
  t->SetBranchAddress("truth_px", &truth_px);
  t->SetBranchAddress("truth_py", &truth_py);
  t->SetBranchAddress("truth_pz", &truth_pz);
  t->SetBranchAddress("Is_shared_sp", &Is_shared_sp);
  t->SetBranchAddress("d_truth_hits", &d_truth_hits);

  //TFile *f_out = new TFile("output/rowID_validation.root","recreate");
  TH1D *h_distance = new TH1D("distance","",200,0,200);
  TH1D *h_distance_shared = new TH1D("distance_shared","",200,0,200);
  TH1D *h_distance_separate = new TH1D("distance_separate","",200,0,200);
  TH2D *h_2D_1 = new TH2D("N_shared_sp_vs_track_momentum","",20,0,1000,10,0,10);
  TH1D *h0_1st_d_p1 = new TH1D("fisrt_plane_distance_p1","",200,0,200);
  TH1D *h1_1st_d_p1 = new TH1D("fisrt_plane_distance_selection1_p1","",200,0,200);
  TH1D *h2_1st_d_p1 = new TH1D("fisrt_plane_distance_selection2_p1","",200,0,200);
  TH1D *h3_1st_d_p1 = new TH1D("fisrt_plane_distance_selection3_p1","",200,0,200);
  TH1D *h1_1st_d_p1_eff = new TH1D("fisrt_plane_distance_selection1_p1_eff","",200,0,200);
  TH1D *h2_1st_d_p1_eff = new TH1D("fisrt_plane_distance_selection2_p1_eff","",200,0,200);
  TH1D *h3_1st_d_p1_eff = new TH1D("fisrt_plane_distance_selection3_p1_eff","",200,0,200);

  int entries=t->GetEntries();
  cout<<"total entries in tree = "<<entries<<endl;
  double first_d, p_total;
  for (int iEntry=0; iEntry<entries; iEntry++)
    {
      t->GetEntry(iEntry);
      if (Is_shared_sp->at(i) > -1) 
       {
      for (int i=0; i<N_plane; i++) 
        {
           h_distance->Fill(d_truth_hit->at(i));
           if (Is_shared_sp->at(i)==1) h_distance_shared->Fill(d_truth_hit->at(i));
           if (Is_shared_sp->at(i)==0) h_distance_separate->Fill(d_truth_hit->at(i));
	}

      first_d = d_truth_hit->at(0);
      p_total = sqrt(pow(truth_px->at(0),2)+pow(truth_py->at(0),2)+pow(truth_pz->at(0),2));
      h_2D_1->Fill(p_total, N_shared_sp);
      if (10<p_total && p_total<50) 
       {
	 h0_1st_d_p1->Fill(first_d);
	 if (N_shared_sp==0) h1_1st_d_p1->Fill(first_d);
	 if (N_shared_sp<2) h2_1st_d_p1->Fill(first_d);
	 if ((N_shared_sp==0) || (N_shared_sp==1 && Is_shared_sp->at(i)==1)) h3_1st_d_p1->Fill(first_d);
       }
       }
    }

  TH1D h_ratio_shared = (*h_distance_shared) / (h_distance);
  TH1D h_ratio_separate = (*h_distance_separate) / (h_distance);

  double h0_c, h0_e, h1_c, h1_e;
  for (int i=1; i<=200; i++)
    {
      h0_c=h0_1st_d_p1->GetBinContent(i);
      h0_e=h0_1st_d_p1->GetBinError(i);
      h1_c=h1_1st_d_p1->GetBinContent(i);
      h1_e=h1_1st_d_p1->GetBinError(i);
      if (h0_c != 0) {
      h1_1st_d_p1_eff.SetBinContent(i, h1_c/h0_c);
      h1_1st_d_p1_eff.SetBinError(i, sqrt(pow(h1_e/h0_c,2.0)+pow(h1_c*h0_e/(h0_c*h0_c),2.0)));
      }
      else {
      h1_1st_d_p1_eff.SetBinContent(i, 0);
      h1_1st_d_p1_eff.SetBinError(i, 0);
      }
      h1_c=h2_1st_d_p1->GetBinContent(i);
      h1_e=h2_1st_d_p1->GetBinError(i);
      if (h0_c != 0) {
      h2_1st_d_p1_eff.SetBinContent(i, h1_c/h0_c);
      h2_1st_d_p1_eff.SetBinError(i, sqrt(pow(h1_e/h0_c,2.0)+pow(h1_c*h0_e/(h0_c*h0_c),2.0)));
      }
      else {
      h2_1st_d_p1_eff.SetBinContent(i, 0);
      h2_1st_d_p1_eff.SetBinError(i, 0);
      }
      h1_c=h3_1st_d_p1->GetBinContent(i);
      h1_e=h3_1st_d_p1->GetBinError(i);
      if (h0_c != 0) {
      h3_1st_d_p1_eff.SetBinContent(i, h1_c/h0_c);
      h3_1st_d_p1_eff.SetBinError(i, sqrt(pow(h1_e/h0_c,2.0)+pow(h1_c*h0_e/(h0_c*h0_c),2.0)));
      }
      else {
      h3_1st_d_p1_eff.SetBinContent(i, 0);
      h3_1st_d_p1_eff.SetBinError(i, 0);
      }

    }

  TCanvas *c1 = new TCanvas("c1","",800,600);
  c1->cd();
  h_ratio_shared->GetXaxis()->SetTitle("Distance [mm]");
  h_ratio_shared->GetYaxis()->SetTitle("Rate of shared space points");
  h_ratio_shared->SetLineWidth(2);
  h_ratio_shared->Draw("hist");
  ATLASLabel(0.2,0.85,"Internal");
  c1->Update();
  c1->SaveAs(Form("plots/%s/sp_separation/Rate_of_shared_space_points.png",name.Data()));

  h_ratio_separate->GetXaxis()->SetTitle("Distance [mm]");
  h_ratio_separate->GetYaxis()->SetTitle("Rate of separate space points");
  h_ratio_separate->SetLineWidth(2);
  h_ratio_separate->Draw("hist");
  ATLASLabel(0.2,0.85,"Internal");
  c1->Update();
  c1->SaveAs(Form("plots/%s/sp_separation/Rate_of_separate_space_points.png",name.Data()));

  h_2D_1->GetXaxis()->SetTitle("Momentum [mm]");
  h_2D_1->GetYaxis()->SetTitle("Number of shared space points");
  h_2D_1->Draw();
  c1->Update();
  c1->SaveAs(Form("plots/%s/sp_separation/N_shared_sp_vs_track_momentum.png",name.Data()));

  h1_1st_d_p1_eff->GetXaxis()->SetTitle("Distance on first plane [mm]");
  h1_1st_d_p1_eff->GetYaxis()->SetTitle("Tracking efficiency");
  h1_1st_d_p1_eff->GetYaxis()->SetRangeUser(0,1.2);
  h1_1st_d_p1_eff->Draw("L,P,E");
  h2_1st_d_p1_eff->Draw("L,P,E,same");
  h3_1st_d_p1_eff->Draw("L,P,E,same");
  h1_1st_d_p1_eff->SetLineColor(kBlack);
  h1_1st_d_p1_eff->SetMarkerColor(kBlack);
  h2_1st_d_p1_eff->SetLineColor(kBlue);
  h2_1st_d_p1_eff->SetMarkerColor(kBlue);
  h3_1st_d_p1_eff->SetLineColor(kRed);
  h3_1st_d_p1_eff->SetMarkerColor(kRed);
  TLegend *leg = new TLegend(0.5, 0.75, 0.9, 0.9);
   leg -> AddEntry(h1_1st_d_p1_eff,"No shared space point", "LPE");
   leg -> AddEntry(h2_1st_d_p1_eff,"Less than 2 shared space point", "LPE");
   leg -> AddEntry(h3_1st_d_p1_eff,"Only 1 shared space point on 1st plane", "LPE");
   leg -> SetFillStyle(0);
   leg -> SetBorderSize(0);
   leg -> SetTextSize(0.04);
  leg->Draw();
  ATLASLabel(0.2,0.85,"Internal");
  c1->Update();
  c1->SaveAs(Form("plots/%s/sp_separation/2particle_efficiency_p1.png",name.Data()));

}
