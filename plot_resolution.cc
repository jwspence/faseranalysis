#include "TTree.h"
#include "TProfile.h"
//#include "AtlasLabels.h"
//#include "AtlasLabels.C"
//#include "AtlasStyle.h"
//#include "AtlasStyle.C"
//#include "AtlasUtils.h"
//#include "AtlasUtils.C"
#include <vector>

double getFitRange(TH1 * h, double median, double heightFraction)
{
  double height = h->GetBinContent(h->FindBin(median));
  double threshold = heightFraction * height;
  int binHigh = 0;
  for (int i=h->FindBin(median); i < h->GetNbinsX(); ++i) {
    if (h->GetBinContent(i) < threshold && binHigh==0) {
      binHigh = i;
      break;
    }
  }
  int binLow = 0;
  for(int i=h->FindBin(median); i > 0; --i){
    if (h->GetBinContent(i) < threshold && binLow==0) {
      binLow = i;
      break;
    }
  }
  double sigmaHigh = h->GetBinCenter(binHigh)-h->GetBinCenter(h->FindBin(median));
  double sigmaLow  = h->GetBinCenter(binLow)-h->GetBinCenter(h->FindBin(median));
  double sigma = 0.5*(sigmaHigh + sigmaLow);
  return sigma;
}


vector<TH1D*> makeResolutionPlot(TH2D * h2D, TString fname) 
{
  TH1D* h1D = new TH1D("res", "", 10, 0, 500);
  TH1D* h1D_2 = new TH1D("mean", "", 10, 0, 500);

  TCanvas c5;
  c5.SetBatch();
  c5.cd();
  TFile check(Form("output/check_%s.root",fname.Data()), "recreate");
  for (int i=1; i<=h2D->GetNbinsX(); ++i) {
    string hname = Form("Res_bin_%i",i);
    TH1D* h = h2D->ProjectionY(hname.c_str(),i,i+1);
    double center = h->GetBinCenter(h->GetMaximumBin());
    double fitRange = 2*h->GetRMS();
    double rMax = center + fitRange;
    double rMin = center - fitRange;
    TF1 * fGauss = new TF1("fGauss", "gaus", rMax, rMin);
    h->Fit("fGauss", "RQL");//use likelihood
    double sigma = fGauss->GetParameter(2);
    double sigmaError = fGauss->GetParError(2);
    fitRange = 2*sigma;
    rMax = fGauss->GetParameter(1)+fitRange;
    rMin = fGauss->GetParameter(1) - fitRange;
    TF1 * fGauss_1 = new TF1("fGauss_1", "gaus", rMax, rMin);
    h->Fit("fGauss_1","RGL");
    sigma = fGauss_1->GetParameter(2);
    sigmaError = fGauss_1->GetParError(2);
    double mean = fGauss_1->GetParameter(1);
    double meanError = fGauss_1->GetParError(1);

    h1D->SetBinContent(i, sigma); // sigma
    h1D->SetBinError(i, sigmaError); // sigma
    h1D_2->SetBinContent(i, mean);
    h1D_2->SetBinError(i, meanError);
    /*
    if (TMath::Abs(sigmaError) < 0.5*TMath::Abs(sigma)) {
      h1D->SetBinError(i, sigmaError); // error on sigma
    }
    */

    check.cd();
    h->Write();
  }
  vector<TH1D*> meanAndSigma;
  meanAndSigma.push_back(h1D_2);
  meanAndSigma.push_back(h1D);
  return meanAndSigma;
}

void plot_resolution()
{
//  SetAtlasStyle();

  //TString name="8plane_muMinus";
  TString name="9plane_muMinus";
  //TString name="9plane_muPlus";
  //const int N_plane=8;
  //double target_z[8]={0, 50, 909.04, 959.04, 1009.04, 1059.04, 1942.02, 1992.02};
  //double target_z[9]={0, 50, 100, 938.03, 988.03, 1038.03, 1892.02, 1942.02, 1992.02};

  //TFile *f = new TFile("helix_tracker_output_242_muMinus.root");
  TFile *f = new TFile("helix_tracker_output_333_muMinus.root");
//  TFile *f = new TFile("helix_tracker_output_333_muPlus.root");
  TTree *t = (TTree*)f->Get("tracks");
  double truth_px, truth_py, truth_pz, truth_R, truth_phi0, truth_lambda, reco_px, reco_py, reco_pz, reco_R, reco_phi0, reco_lambda;
  double chi_square;
  int pdgID, N_track_points;
    std::vector<double> *space_points_x;
    std::vector<double> *space_points_y;
    std::vector<double> *space_points_z;
    std::vector<double> *track_points_x;
    std::vector<double> *track_points_y;
    std::vector<double> *track_points_z;
    std::vector<int> *track_points_rowID;
    std::vector<double> *truth_x;
    std::vector<double> *truth_y;
    std::vector<double> *truth_z;
    std::vector<double> *reco_x;
    std::vector<double> *reco_y;
    std::vector<double> *reco_z;
  t->SetBranchAddress("chi_square", &chi_square);
  t->SetBranchAddress("pdgID", &pdgID);
  t->SetBranchAddress("N_track_points", &N_track_points);
  t->SetBranchAddress("truth_px", &truth_px);
  t->SetBranchAddress("truth_py", &truth_py);
  t->SetBranchAddress("truth_pz", &truth_pz);
  t->SetBranchAddress("truth_R", &truth_R);
  t->SetBranchAddress("truth_phi0", &truth_phi0);
  t->SetBranchAddress("truth_lambda", &truth_lambda);
  t->SetBranchAddress("reco_px", &reco_px);
  t->SetBranchAddress("reco_py", &reco_py);
  t->SetBranchAddress("reco_pz", &reco_pz);
  t->SetBranchAddress("reco_R", &reco_R);
  t->SetBranchAddress("reco_phi0", &reco_phi0);
  t->SetBranchAddress("reco_lambda", &reco_lambda);
  t->SetBranchAddress("truth_x", &truth_x);
  t->SetBranchAddress("truth_y", &truth_y);
  t->SetBranchAddress("truth_z", &truth_z);
  t->SetBranchAddress("reco_x", &reco_x);
  t->SetBranchAddress("reco_y", &reco_y);
  t->SetBranchAddress("reco_z", &reco_z);
  t->SetBranchAddress("space_points_x", &space_points_x);
  t->SetBranchAddress("space_points_y", &space_points_y);
  t->SetBranchAddress("space_points_z", &space_points_z);
  t->SetBranchAddress("track_points_x", &track_points_x);
  t->SetBranchAddress("track_points_y", &track_points_y);
  t->SetBranchAddress("track_points_z", &track_points_z);
  t->SetBranchAddress("track_points_rowID", &track_points_rowID);
// for Gaussian fit
#if 1
  TFile *f_out = new TFile(Form("output/JS_resolution.root",name.Data()),"recreate");
  TH2D *h_p_perp = new TH2D("p_perp","",10,0,500,1000,-20,20);
  TH2D *h_x0 = new TH2D("x0","",10,0,500,500,-10,10);
  TH2D *h_y0 = new TH2D("y0","",10,0,500,100,-10,10);
  TH2D *h_phi0 = new TH2D("phi0","",10,0,500,600,-0.03,0.03);
  TH2D *h_lambda = new TH2D("lambda","",10,0,500,600,-0.03,0.03);
#endif
// for Profile
#if 0
  TH2D *h_p_perp = new TH2D("p_perp","",10,0,500,400000,-20,20);
  TH2D *h_x0 = new TH2D("x0","",10,0,500,10000,-10,10);
  TH2D *h_y0 = new TH2D("y0","",10,0,500,10000,-10,10);
  TH2D *h_phi0 = new TH2D("phi0","",10,0,500,6000,-0.003,0.003);
  TH2D *h_lambda = new TH2D("lambda","",10,0,500,6000,-0.003,0.003);
#endif
  double truth_pperp, truth_p, reco_pperp, reco_p;
  for (int iEntry=0; iEntry < t->GetEntries(); iEntry++)
    {
      t->GetEntry(iEntry);
      if (chi_square/(N_track_points*2-5)<30) 
      {
      truth_pperp = sqrt(truth_px*truth_px + truth_pz*truth_pz);
      reco_pperp = sqrt(reco_px*reco_px + reco_pz*reco_pz);

      h_p_perp->Fill(truth_pperp, (reco_pperp-truth_pperp)/truth_pperp);
      h_x0->Fill(truth_pperp, reco_x->at(0) - truth_x->at(0));
      h_y0->Fill(truth_pperp, reco_y->at(0) - truth_y->at(0));
      h_phi0->Fill(truth_pperp, reco_phi0-truth_phi0);
      h_lambda->Fill(truth_pperp, reco_lambda-truth_lambda);
/*
      profile1->Fill(truth_pperp, (reco_pperp - truth_pperp)/truth_pperp);
      truth_p = sqrt(truth_px*truth_px + truth_py*truth_py + truth_pz*truth_pz);
      reco_p = sqrt(reco_px*reco_px + reco_py*reco_py + reco_pz*reco_pz);
      profile2->Fill(truth_p, (reco_p - truth_p)/truth_p);
*/
      }
    }
  vector<TH1D*> p_perp_meanAndSigma = makeResolutionPlot(h_p_perp,Form("p_perp_%s",name.Data()));
  TH1D* h_p_perp_Mean = p_perp_meanAndSigma[0];
  TH1D* h_p_perp_Sigma = p_perp_meanAndSigma[1];
  h_p_perp_Mean->SetName("p_perp_mean");
  h_p_perp_Sigma->SetName("p_perp_reso");
  vector<TH1D*> x0_meanAndSigma = makeResolutionPlot(h_x0,Form("x0_%s",name.Data()));
  TH1D* h_x0_Mean = x0_meanAndSigma[0];
  TH1D* h_x0_Sigma = x0_meanAndSigma[1];
  h_x0_Mean->SetName("x0_mean");
  h_x0_Sigma->SetName("x0_reso");
  vector<TH1D*> y0_meanAndSigma = makeResolutionPlot(h_y0,Form("y0_%s",name.Data()));
  TH1D* h_y0_Mean = y0_meanAndSigma[0];
  TH1D* h_y0_Sigma = y0_meanAndSigma[1];
  h_y0_Mean->SetName("y0_mean");
  h_y0_Sigma->SetName("y0_reso");
  vector<TH1D*> phi0_meanAndSigma = makeResolutionPlot(h_phi0,Form("phi0_%s",name.Data()));
  TH1D* h_phi0_Mean = phi0_meanAndSigma[0];
  TH1D* h_phi0_Sigma = phi0_meanAndSigma[1];
  h_phi0_Mean->SetName("phi0_mean");
  h_phi0_Sigma->SetName("phi0_reso");
  vector<TH1D*> lambda_meanAndSigma = makeResolutionPlot(h_lambda,Form("lambda_%s",name.Data()));
  TH1D* h_lambda_Mean = lambda_meanAndSigma[0];
  TH1D* h_lambda_Sigma = lambda_meanAndSigma[1];
  h_lambda_Mean->SetName("lambda_mean");
  h_lambda_Sigma->SetName("lambda_reso");
  

  f_out->cd();
  h_p_perp->Write();
  h_x0->Write();
  h_y0->Write();
  h_phi0->Write();
  h_lambda->Write();
  h_p_perp_Mean->Write();
  h_x0_Mean->Write();
  h_y0_Mean->Write();
  h_phi0_Mean->Write();
  h_lambda_Mean->Write();
  h_p_perp_Sigma->Write();
  h_x0_Sigma->Write();
  h_y0_Sigma->Write();
  h_phi0_Sigma->Write();
  h_lambda_Sigma->Write();
  f_out->Close();

/*
  TProfile *p_p_perp = h_p_perp->ProfileX("p_perp_profile");
  TProfile *p_x0 = h_x0->ProfileX("x0_profile");
  TProfile *p_y0 = h_y0->ProfileX("y0_profile");
  TProfile *p_phi0 = h_phi0->ProfileX("phi0_profile");
  TProfile *p_lambda = h_lambda->ProfileX("lambda_profile");
  TH1D *r_p_perp = new TH1D("p_perp_reso","",10,0,500);
  TH1D *r_x0 = new TH1D("x0_reso","",10,0,500);
  TH1D *r_y0 = new TH1D("y0_reso","",10,0,500);
  TH1D *r_phi0 = new TH1D("phi0_reso","",10,0,500);
  TH1D *r_lambda = new TH1D("lambda_reso","",10,0,500);
  for (int i=1; i<=10; i++)
    {
      r_p_perp->SetBinContent(i, p_p_perp->GetBinError(i));
      r_p_perp->SetBinError(i, 1e-10);
      r_x0->SetBinContent(i, p_x0->GetBinError(i));
      r_x0->SetBinError(i, 1e-10);
      r_y0->SetBinContent(i, p_y0->GetBinError(i));
      r_y0->SetBinError(i, 1e-10);
      r_phi0->SetBinContent(i, p_phi0->GetBinError(i));
      r_phi0->SetBinError(i, 1e-10);
      r_lambda->SetBinContent(i, p_lambda->GetBinError(i));
      r_lambda->SetBinError(i, 1e-10);
    }

  TCanvas *c1 = new TCanvas("c1","",800,600);
  c1->cd();
  h_p_perp->GetXaxis()->SetTitle("p_{T, truth} [GeV]");
  h_p_perp->GetYaxis()->SetTitle("(p_{T, reco}-p_{T, truth}) / p_{T, truth}");
  h_p_perp->SetMarkerSize(0.5);
  h_p_perp->Draw("");
  ATLASLabel(0.2,0.85,"Internal");
  c1->Update();
  c1->SaveAs(Form("plots/%s/pT_resolution.png",name.Data()));
  h_x0->GetXaxis()->SetTitle("p_{T, truth} [GeV]");
  h_x0->GetYaxis()->SetTitle("X_{0, reco}-X_{0, truth} [mm]");
  h_x0->SetMarkerSize(0.5);
  h_x0->Draw();
  ATLASLabel(0.2,0.85,"Internal");
  c1->Update();
  c1->SaveAs(Form("plots/%s/X0_resolution.png",name.Data()));
  h_y0->GetXaxis()->SetTitle("p_{T, truth} [GeV]");
  h_y0->GetYaxis()->SetTitle("Y_{0, reco}-Y_{0, truth} [mm]");
  h_y0->SetMarkerSize(0.5);
  h_y0->Draw();
  ATLASLabel(0.2,0.85,"Internal");
  c1->Update();
  c1->SaveAs(Form("plots/%s/Y0_resolution.png",name.Data()));
  h_phi0->GetXaxis()->SetTitle("p_{T, truth} [GeV]");
  h_phi0->GetYaxis()->SetTitle("#phi_{0, reco}-#phi_{0, truth} [rad]");
  h_phi0->SetMarkerSize(0.5);
  h_phi0->Draw();
  ATLASLabel(0.2,0.85,"Internal");
  c1->Update();
  c1->SaveAs(Form("plots/%s/phi0_resolution.png",name.Data()));
  h_lambda->GetXaxis()->SetTitle("p_{T, truth} [GeV]");
  h_lambda->GetYaxis()->SetTitle("#lambda_{reco}-#lambda_{truth} [rad]");
  h_lambda->SetMarkerSize(0.5);
  h_lambda->Draw();
  ATLASLabel(0.2,0.85,"Internal");
  c1->Update();
  c1->SaveAs(Form("plots/%s/lambda_resolution.png",name.Data()));
  p_p_perp->GetXaxis()->SetTitle("p_{T, truth} [GeV]");
  p_p_perp->GetYaxis()->SetTitle("(p_{T, reco}-p_{T, truth}) / p_{T, truth}");
  p_p_perp->Draw("L,P,E1");
  ATLASLabel(0.2,0.85,"Internal");
  c1->Update();
  c1->SaveAs(Form("plots/%s/pT_resolution_profile.png",name.Data()));
  p_x0->GetXaxis()->SetTitle("p_{T, truth} [GeV]");
  p_x0->GetYaxis()->SetTitle("X_{0, reco}-X_{0, truth} [mm]");
  p_x0->Draw("L,P,E1");
  ATLASLabel(0.2,0.85,"Internal");
  c1->Update();
  c1->SaveAs(Form("plots/%s/X0_resolution_profile.png",name.Data()));
  p_y0->GetXaxis()->SetTitle("p_{T, truth} [GeV]");
  p_y0->GetYaxis()->SetTitle("Y_{0, reco}-Y_{0, truth} [mm]");
  p_y0->Draw("L,P,E1");
  ATLASLabel(0.2,0.85,"Internal");
  c1->Update();
  c1->SaveAs(Form("plots/%s/Y0_resolution_profile.png",name.Data()));
  p_phi0->GetXaxis()->SetTitle("p_{T, truth} [GeV]");
  p_phi0->GetYaxis()->SetTitle("#phi_{0, reco}-#phi_{0, truth} [rad]");
  p_phi0->Draw("L,P,E1");
  ATLASLabel(0.2,0.85,"Internal");
  c1->Update();
  c1->SaveAs(Form("plots/%s/phi0_resolution_profile.png",name.Data()));
  p_lambda->GetXaxis()->SetTitle("p_{T, truth} [GeV]");
  p_lambda->GetYaxis()->SetTitle("#lambda_{reco}-#lambda_{truth} [rad]");
  p_lambda->Draw("L,P,E1");
  ATLASLabel(0.2,0.85,"Internal");
  c1->Update();
  c1->SaveAs(Form("plots/%s/lambda_resolution_profile.png",name.Data()));

  f_out->cd();
  h_p_perp->Write();
  h_x0->Write();
  h_y0->Write();
  h_phi0->Write();
  h_lambda->Write();
  p_p_perp->Write();
  p_x0->Write();
  p_y0->Write();
  p_phi0->Write();
  p_lambda->Write();
  r_p_perp->Write();
  r_x0->Write();
  r_y0->Write();
  r_phi0->Write();
  r_lambda->Write();
  f_out->Close();
  */
}
