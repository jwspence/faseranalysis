#include "TTree.h"
#include "TProfile.h"
#include "AtlasLabels.h"
#include "AtlasLabels.C"
#include "AtlasStyle.h"
#include "AtlasStyle.C"
#include "AtlasUtils.h"
#include "AtlasUtils.C"

void plot_others()
{
  TString name="TP_muMinus";
  //TString name="9plane_muMinus";
  //const int N_plane=9;
  //double target_z[8]={0, 50, 909.04, 959.04, 1009.04, 1059.04, 1942.02, 1992.02};
  //double target_z[9]={0, 50, 100, 938.03, 988.03, 1038.03, 1892.02, 1942.02, 1992.02};

  //TFile *f = new TFile("helix_tracker_output.root");
  //TFile *f = new TFile("helix_tracker_output_242_muMinus.root");
  TFile *f = new TFile("helix_tracker_TP_output_muMinus.root");
  TTree *t = (TTree*)f->Get("tracks");
  double truth_px, truth_py, truth_pz, truth_R, truth_phi0, truth_lambda, reco_px, reco_py, reco_pz, reco_R, reco_phi0, reco_lambda;
  double chi_square;
  int pdgID, N_track_points;
  int N_shared_sp,status;
    std::vector<double> *space_points_x;
    std::vector<double> *space_points_y;
    std::vector<double> *space_points_z;
    std::vector<double> *track_points_x;
    std::vector<double> *track_points_y;
    std::vector<double> *track_points_z;
    std::vector<int> *track_points_rowID;
    std::vector<double> *truth_x;
    std::vector<double> *truth_y;
    std::vector<double> *truth_z;
    std::vector<double> *reco_x;
    std::vector<double> *reco_y;
    std::vector<double> *reco_z;
    std::vector<int> *Is_shared_sp;
  t->SetBranchAddress("chi_square", &chi_square);
  t->SetBranchAddress("pdgID", &pdgID);
  t->SetBranchAddress("N_track_points", &N_track_points);
  t->SetBranchAddress("N_shared_sp", &N_shared_sp);
  t->SetBranchAddress("status", &status);
  t->SetBranchAddress("truth_px", &truth_px);
  t->SetBranchAddress("truth_py", &truth_py);
  t->SetBranchAddress("truth_pz", &truth_pz);
  t->SetBranchAddress("truth_R", &truth_R);
  t->SetBranchAddress("truth_phi0", &truth_phi0);
  t->SetBranchAddress("truth_lambda", &truth_lambda);
  t->SetBranchAddress("reco_px", &reco_px);
  t->SetBranchAddress("reco_py", &reco_py);
  t->SetBranchAddress("reco_pz", &reco_pz);
  t->SetBranchAddress("reco_R", &reco_R);
  t->SetBranchAddress("reco_phi0", &reco_phi0);
  t->SetBranchAddress("reco_lambda", &reco_lambda);
  t->SetBranchAddress("truth_x", &truth_x);
  t->SetBranchAddress("truth_y", &truth_y);
  t->SetBranchAddress("truth_z", &truth_z);
  t->SetBranchAddress("reco_x", &reco_x);
  t->SetBranchAddress("reco_y", &reco_y);
  t->SetBranchAddress("reco_z", &reco_z);
  t->SetBranchAddress("space_points_x", &space_points_x);
  t->SetBranchAddress("space_points_y", &space_points_y);
  t->SetBranchAddress("space_points_z", &space_points_z);
  t->SetBranchAddress("track_points_x", &track_points_x);
  t->SetBranchAddress("track_points_y", &track_points_y);
  t->SetBranchAddress("track_points_z", &track_points_z);
  t->SetBranchAddress("track_points_rowID", &track_points_rowID);
  t->SetBranchAddress("Is_shared_sp", &Is_shared_sp);

  //TFile *f_out = new TFile("output/rowID_validation.root","recreate");
  TH1D *h_chi2 = new TH1D("chi2","",200,0,100);

  int entries=t->GetEntries();
  cout<<"total entries in tree = "<<entries<<endl;
  for (int iEntry=0; iEntry<entries; iEntry++)
    {
      t->GetEntry(iEntry);
      h_chi2->Fill(chi_square/(N_track_points*2-5));
    }

  int nbins = h_chi2->GetNbinsX();
  if (h_chi2->IsBinOverflow(nbins+1)) 
  {
  double over = h_chi2->GetBinContent(nbins+1);
  double err_over = h_chi2->GetBinError(nbins+1);
  h_chi2->SetBinContent(nbins, h_chi2->GetBinContent(nbins) + over);
  h_chi2->SetBinError(nbins, sqrt(h_chi2->GetBinError(nbins)*h_chi2->GetBinError(nbins) + err_over*err_over));
  }
  cout<<"The bin content of the last bin of chi2/dof = "<<h_chi2->GetBinContent(nbins)<<endl;

  TCanvas *c1 = new TCanvas("c1","",800,600);
  c1->cd();
  h_chi2->GetXaxis()->SetTitle("#chi^{2}/dof");
  h_chi2->GetYaxis()->SetTitle("Events");
  h_chi2->SetLineWidth(2);
  h_chi2->Draw();
  c1->Update();
  c1->SaveAs(Form("plots/%s/chi2_over_dof.png",name.Data()));
}
