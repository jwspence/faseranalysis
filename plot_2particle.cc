#include "TTree.h"
#include "TProfile.h"
#include "AtlasLabels.h"
#include "AtlasLabels.C"
#include "AtlasStyle.h"
#include "AtlasStyle.C"
#include "AtlasUtils.h"
#include "AtlasUtils.C"

void plot_2particle()
{
  TString name="9plane_muMinus";
  const int N_plane=9;
  //double target_z[8]={0, 50, 909.04, 959.04, 1009.04, 1059.04, 1942.02, 1992.02};
  //double target_z[9]={0, 50, 100, 938.03, 988.03, 1038.03, 1892.02, 1942.02, 1992.02};

  //TFile *f = new TFile("helix_tracker_output.root");
  //TFile *f = new TFile("helix_tracker_output_242_muMinus.root");
  TFile *f = new TFile("helix_tracker_output_333_muMinus.root");
  TTree *t = (TTree*)f->Get("tracks");
  double truth_px, truth_py, truth_pz, truth_R, truth_phi0, truth_lambda, reco_px, reco_py, reco_pz, reco_R, reco_phi0, reco_lambda;
  double chi_square;
  int pdgID, N_track_points;
  int N_shared_sp,status;
    std::vector<double> *space_points_x;
    std::vector<double> *space_points_y;
    std::vector<double> *space_points_z;
    std::vector<double> *track_points_x;
    std::vector<double> *track_points_y;
    std::vector<double> *track_points_z;
    std::vector<int> *track_points_rowID;
    std::vector<double> *truth_x;
    std::vector<double> *truth_y;
    std::vector<double> *truth_z;
    std::vector<double> *reco_x;
    std::vector<double> *reco_y;
    std::vector<double> *reco_z;
    std::vector<int> *Is_shared_sp;
  t->SetBranchAddress("chi_square", &chi_square);
  t->SetBranchAddress("pdgID", &pdgID);
  t->SetBranchAddress("N_track_points", &N_track_points);
  t->SetBranchAddress("N_shared_sp", &N_shared_sp);
  t->SetBranchAddress("status", &status);
  t->SetBranchAddress("truth_px", &truth_px);
  t->SetBranchAddress("truth_py", &truth_py);
  t->SetBranchAddress("truth_pz", &truth_pz);
  t->SetBranchAddress("truth_R", &truth_R);
  t->SetBranchAddress("truth_phi0", &truth_phi0);
  t->SetBranchAddress("truth_lambda", &truth_lambda);
  t->SetBranchAddress("reco_px", &reco_px);
  t->SetBranchAddress("reco_py", &reco_py);
  t->SetBranchAddress("reco_pz", &reco_pz);
  t->SetBranchAddress("reco_R", &reco_R);
  t->SetBranchAddress("reco_phi0", &reco_phi0);
  t->SetBranchAddress("reco_lambda", &reco_lambda);
  t->SetBranchAddress("truth_x", &truth_x);
  t->SetBranchAddress("truth_y", &truth_y);
  t->SetBranchAddress("truth_z", &truth_z);
  t->SetBranchAddress("reco_x", &reco_x);
  t->SetBranchAddress("reco_y", &reco_y);
  t->SetBranchAddress("reco_z", &reco_z);
  t->SetBranchAddress("space_points_x", &space_points_x);
  t->SetBranchAddress("space_points_y", &space_points_y);
  t->SetBranchAddress("space_points_z", &space_points_z);
  t->SetBranchAddress("track_points_x", &track_points_x);
  t->SetBranchAddress("track_points_y", &track_points_y);
  t->SetBranchAddress("track_points_z", &track_points_z);
  t->SetBranchAddress("track_points_rowID", &track_points_rowID);
  t->SetBranchAddress("Is_shared_sp", &Is_shared_sp);

  //TFile *f_out = new TFile("output/rowID_validation.root","recreate");
  TH1D *h_distance = new TH1D("distance","",200,0,200);
  TH1D *h_distance_shared = new TH1D("distance_shared","",200,0,200);
  TH1D *h_distance_separate = new TH1D("distance_separate","",200,0,200);
  TH2D *h_2D_1 = new TH2D("N_shared_sp_vs_track_momentum","",20,0,1000,10,0,10);
  TH1D *h0_1st_d_p1 = new TH1D("fisrt_plane_distance_p1","",200,0,200);
  TH1D *h1_1st_d_p1 = new TH1D("fisrt_plane_distance_selection1_p1","",200,0,200);
  TH1D *h2_1st_d_p1 = new TH1D("fisrt_plane_distance_selection2_p1","",200,0,200);
  TH1D *h3_1st_d_p1 = new TH1D("fisrt_plane_distance_selection3_p1","",200,0,200);
  TH1D *h1_1st_d_p1_eff = new TH1D("fisrt_plane_distance_selection1_p1_eff","",200,0,200);
  TH1D *h2_1st_d_p1_eff = new TH1D("fisrt_plane_distance_selection2_p1_eff","",200,0,200);
  TH1D *h3_1st_d_p1_eff = new TH1D("fisrt_plane_distance_selection3_p1_eff","",200,0,200);

  int entries=t->GetEntries();
  cout<<"total entries in tree = "<<entries<<endl;
  int first_status;
  vector<double> *first_truth_x = new vector<double>;
  vector<double> *first_truth_y = new vector<double>;
  double first_d, p_total;
  for (int iEntry=0; iEntry<entries/2; iEntry++)
    {
      t->GetEntry(iEntry*2);
      if (status == -1) continue;
      for (int i=0; i<N_plane; i++) {
        first_truth_x->push_back(truth_x->at(i));
        first_truth_y->push_back(truth_y->at(i));

      t->GetEntry(iEntry*2+1);
      if (status == -1) continue;
      for (int i=0; i<N_plane; i++) 
        {
           h_distance->Fill(sqrt(pow(truth_x->at(i)-first_truth_x->at(i),2)+pow(truth_y->at(i)-first_truth_y->at(i),2)));
           if (Is_shared_sp->at(i)==1) h_distance_shared->Fill(sqrt(pow(truth_x->at(i)-first_truth_x->at(i),2)+pow(truth_y->at(i)-first_truth_y->at(i),2)));
           if (Is_shared_sp->at(i)==0) h_distance_separate->Fill(sqrt(pow(truth_x->at(i)-first_truth_x->at(i),2)+pow(truth_y->at(i)-first_truth_y->at(i),2)));
	}

      first_d = sqrt(pow(truth_x->at(0)-first_truth_x->at(0),2)+pow(truth_y->at(0)-first_truth_y->at(0),2));
      p_total = sqrt(pow(truth_px,2)+pow(truth_py,2)+pow(truth_pz,2));
      h_2D_1->Fill(p_total, N_shared_sp);
      if (10<p_total && p_total<50) 
       {
	 h0_1st_d_p1->Fill(first_d);
	 if (N_shared_sp==0) h1_1st_d_p1->Fill(first_d);
	 if (N_shared_sp<2) h2_1st_d_p1->Fill(first_d);
	 if ((N_shared_sp==0) || (N_shared_sp==1 && Is_shared_sp->at(i)==1)) h3_1st_d_p1->Fill(first_d);
       }
    }

  TH1D h_ratio_shared = (*h_distance_shared) / (h_distance);
  TH1D h_ratio_separate = (*h_distance_separate) / (h_distance);

  double h0_c, h0_e, h1_c, h1_e;
  for (int i=1; i<=200; i++)
    {
      h0_c=h0_1st_d_p1->GetBinContent(i);
      h0_e=h0_1st_d_p1->GetBinError(i);
      h1_c=h1_1st_d_p1->GetBinContent(i);
      h1_e=h1_1st_d_p1->GetBinError(i);
      if (h0_c != 0) {
      h1_1st_d_p1_eff.SetBinContent(i, h1_c/h0_c);
      h1_1st_d_p1_eff.SetBinError(i, sqrt(pow(h1_e/h0_c,2.0)+pow(h1_c*h0_e/(h0_c*h0_c),2.0)));
      }
      else {
      h1_1st_d_p1_eff.SetBinContent(i, 0);
      h1_1st_d_p1_eff.SetBinError(i, 0);
      }
      h1_c=h2_1st_d_p1->GetBinContent(i);
      h1_e=h2_1st_d_p1->GetBinError(i);
      if (h0_c != 0) {
      h2_1st_d_p1_eff.SetBinContent(i, h1_c/h0_c);
      h2_1st_d_p1_eff.SetBinError(i, sqrt(pow(h1_e/h0_c,2.0)+pow(h1_c*h0_e/(h0_c*h0_c),2.0)));
      }
      else {
      h2_1st_d_p1_eff.SetBinContent(i, 0);
      h2_1st_d_p1_eff.SetBinError(i, 0);
      }
      h1_c=h3_1st_d_p1->GetBinContent(i);
      h1_e=h3_1st_d_p1->GetBinError(i);
      if (h0_c != 0) {
      h3_1st_d_p1_eff.SetBinContent(i, h1_c/h0_c);
      h3_1st_d_p1_eff.SetBinError(i, sqrt(pow(h1_e/h0_c,2.0)+pow(h1_c*h0_e/(h0_c*h0_c),2.0)));
      }
      else {
      h3_1st_d_p1_eff.SetBinContent(i, 0);
      h3_1st_d_p1_eff.SetBinError(i, 0);
      }

    }

  TCanvas *c1 = new TCanvas("c1","",800,600);
  c1->cd();
  h_ratio_shared->GetXaxis()->SetTitle("Distance [mm]");
  h_ratio_shared->GetYaxis()->SetTitle("Rate of shared space points");
  h_ratio_shared->SetLineWidth(2);
  h_ratio_shared->Draw("hist");
  ATLASLabel(0.2,0.85,"Internal");
  c1->Update();
  c1->SaveAs(Form("plots/%s/Rate_of_shared_space_points.png",name.Data()));

  h_ratio_separate->GetXaxis()->SetTitle("Distance [mm]");
  h_ratio_separate->GetYaxis()->SetTitle("Rate of separate space points");
  h_ratio_separate->SetLineWidth(2);
  h_ratio_separate->Draw("hist");
  ATLASLabel(0.2,0.85,"Internal");
  c1->Update();
  c1->SaveAs(Form("plots/%s/Rate_of_separate_space_points.png",name.Data()));

  h_2D_1->GetXaxis()->SetTitle("Momentum [mm]");
  h_2D_1->GetYaxis()->SetTitle("Number of shared space points");
  h_2D_1->Draw();
  c1->Update();
  c1->SaveAs(Form("plots/%s/N_shared_sp_vs_track_momentum.png",name.Data()));

  h1_1st_d_p1_eff->GetXaxis()->SetTitle("Distance on first plane [mm]");
  h1_1st_d_p1_eff->GetYaxis()->SetTitle("Tracking efficiency");
  h1_1st_d_p1_eff->GetYaxis()->SetRangeUser(0,1.2);
  h1_1st_d_p1_eff->Draw("L,P,E");
  h2_1st_d_p1_eff->Draw("L,P,E,same");
  h3_1st_d_p1_eff->Draw("L,P,E,same");
  h1_1st_d_p1_eff->SetLineColor(kBlack);
  h1_1st_d_p1_eff->SetMarkerColor(kBlack);
  h2_1st_d_p1_eff->SetLineColor(kBlue);
  h2_1st_d_p1_eff->SetMarkerColor(kBlue);
  h3_1st_d_p1_eff->SetLineColor(kRed);
  h3_1st_d_p1_eff->SetMarkerColor(kRed);
  TLegend *leg = new TLegend(0.5, 0.75, 0.9, 0.9);
   leg -> AddEntry(h1_1st_d_p1_eff,"No shared space point", "LPE");
   leg -> AddEntry(h2_1st_d_p1_eff,"Less than 2 shared space point", "LPE");
   leg -> AddEntry(h3_1st_d_p1_eff,"Only 1 shared space point on 1st plane", "LPE");
   leg -> SetFillStyle(0);
   leg -> SetBorderSize(0);
   leg -> SetTextSize(0.04);
  leg->Draw();
  ATLASLabel(0.2,0.85,"Internal");
  c1->Update();
  c1->SaveAs(Form("plots/%s/2particle_efficiency_p1.png",name.Data()));

}
