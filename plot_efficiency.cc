#include "TTree.h"
#include "TProfile.h"
#include "AtlasLabels.h"
#include "AtlasLabels.C"
#include "AtlasStyle.h"
#include "AtlasStyle.C"
#include "AtlasUtils.h"
#include "AtlasUtils.C"

void plot_efficiency()
{
  SetAtlasStyle();

  //double target_z[8]={0, 50, 909.04, 959.04, 1009.04, 1059.04, 1942.02, 1992.02};
  //TString name="8plane_muMinus";
  TString name="9plane_muMinus";
#if 0
  //TString hname="no_cut";
  //TString hname="4points_cut";
  TString hname="plane7_cut";

  //TFile *f = new TFile(Form("track_efficiency_%s_242_muMinus.root",hname.Data()));
  TFile *f = new TFile(Form("track_efficiency_%s_333_muMinus.root",hname.Data()));
  TTree *t = (TTree*)f->Get("tracks");
  double truth_px, truth_py, truth_pz, truth_R, truth_phi0, truth_lambda, reco_px, reco_py, reco_pz, reco_R, reco_phi0, reco_lambda;
  double chi_square;
  int pdgID, N_track_points;
    std::vector<double> *space_points_x;
    std::vector<double> *space_points_y;
    std::vector<double> *space_points_z;
    std::vector<double> *track_points_x;
    std::vector<double> *track_points_y;
    std::vector<double> *track_points_z;
    std::vector<int> *track_points_rowID;
    std::vector<double> *truth_x;
    std::vector<double> *truth_y;
    std::vector<double> *truth_z;
    std::vector<double> *reco_x;
    std::vector<double> *reco_y;
    std::vector<double> *reco_z;
  t->SetBranchAddress("chi_square", &chi_square);
  t->SetBranchAddress("pdgID", &pdgID);
  t->SetBranchAddress("N_track_points", &N_track_points);
  t->SetBranchAddress("truth_px", &truth_px);
  t->SetBranchAddress("truth_py", &truth_py);
  t->SetBranchAddress("truth_pz", &truth_pz);
  t->SetBranchAddress("truth_R", &truth_R);
  t->SetBranchAddress("truth_phi0", &truth_phi0);
  t->SetBranchAddress("truth_lambda", &truth_lambda);
  t->SetBranchAddress("reco_px", &reco_px);
  t->SetBranchAddress("reco_py", &reco_py);
  t->SetBranchAddress("reco_pz", &reco_pz);
  t->SetBranchAddress("reco_R", &reco_R);
  t->SetBranchAddress("reco_phi0", &reco_phi0);
  t->SetBranchAddress("reco_lambda", &reco_lambda);
  t->SetBranchAddress("truth_x", &truth_x);
  t->SetBranchAddress("truth_y", &truth_y);
  t->SetBranchAddress("truth_z", &truth_z);
  t->SetBranchAddress("reco_x", &reco_x);
  t->SetBranchAddress("reco_y", &reco_y);
  t->SetBranchAddress("reco_z", &reco_z);
  t->SetBranchAddress("space_points_x", &space_points_x);
  t->SetBranchAddress("space_points_y", &space_points_y);
  t->SetBranchAddress("space_points_z", &space_points_z);
  t->SetBranchAddress("track_points_x", &track_points_x);
  t->SetBranchAddress("track_points_y", &track_points_y);
  t->SetBranchAddress("track_points_z", &track_points_z);
  t->SetBranchAddress("track_points_rowID", &track_points_rowID);

  //TFile *f_out = new TFile(Form("output/%s/efficiency.root",name.Data()),"recreate");
  TFile *f_out = new TFile(Form("output/%s/efficiency.root",name.Data()),"update");
  TH1D *h_pper = new TH1D(Form("pper_%s",hname.Data()),"",10,0,500);
  TH1D *h_x0 = new TH1D(Form("x0_%s",hname.Data()),"",10,-100,100);
  TH1D *h_y0 = new TH1D(Form("y0_%s",hname.Data()),"",10,-100,100);
  TH1D *h_phi0 = new TH1D(Form("phi0_%s",hname.Data()),"",30,-3.2,3.2);
  TH1D *h_lambda = new TH1D(Form("lambda_%s",hname.Data()),"",30,-3.2,3.2);

  int entries=t->GetEntries();
  cout<<"total entries in tree = "<<entries<<endl;
  for (int iEntry=0; iEntry<entries; iEntry++)
    {
      t->GetEntry(iEntry);
      h_pper->Fill(sqrt(truth_px*truth_px+truth_pz*truth_pz));
      h_x0->Fill(truth_x->at(0));
      h_y0->Fill(truth_y->at(0));
      h_phi0->Fill(truth_phi0);
      h_lambda->Fill(truth_lambda);
    }
  f_out->cd();
  h_pper->Write();
  h_x0->Write();
  h_y0->Write();
  h_phi0->Write();
  h_lambda->Write();
  f_out->Close();
#endif
#if 1
  //TString hname="4points";
  TString hname="plane7";
  TFile *f = new TFile(Form("output/%s/efficiency.root",name.Data()),"update");
  TH1D *h0_pper = (TH1D*)f->Get("pper_no_cut");
  TH1D *h0_x0 = (TH1D*)f->Get("x0_no_cut");
  TH1D *h0_y0 = (TH1D*)f->Get("y0_no_cut");
  TH1D *h0_phi0 = (TH1D*)f->Get("phi0_no_cut");
  TH1D *h0_lambda = (TH1D*)f->Get("lambda_no_cut");

  TH1D *h1_pper = (TH1D*)f->Get(Form("pper_%s_cut",hname.Data()));
  TH1D *h1_x0 = (TH1D*)f->Get(Form("x0_%s_cut",hname.Data()));
  TH1D *h1_y0 = (TH1D*)f->Get(Form("y0_%s_cut",hname.Data()));
  TH1D *h1_phi0 = (TH1D*)f->Get(Form("phi0_%s_cut",hname.Data()));
  TH1D *h1_lambda = (TH1D*)f->Get(Form("lambda_%s_cut",hname.Data()));

/*
  TH1D h2_pper = (*h1_pper) / (*h0_pper);
  TH1D h2_x0 = (*h1_x0) / (*h0_x0);
  TH1D h2_y0 = (*h1_y0) / (*h0_y0);
  TH1D h2_phi0 = (*h1_phi0) / (*h0_phi0);
  TH1D h2_lambda = (*h1_lambda) / (*h0_lambda);
*/
  TH1D h2_pper("pper_eff","",10,0,500);
  TH1D h2_x0("x0_eff","",10,-100,100);
  TH1D h2_y0("y0_eff","",10,-100,100);
  TH1D h2_phi0("phi0_eff","",30,-3.2,3.2);
  TH1D h2_lambda("lambda_eff","",30,-3.2,3.2);
  double h0_c=0, h0_e=0, h1_c=0, h1_e=0;
  for (int i=1; i<=10; i++)
    {
      h0_c=h0_pper->GetBinContent(i);
      h0_e=h0_pper->GetBinError(i);
      h1_c=h1_pper->GetBinContent(i);
      h1_e=h1_pper->GetBinError(i);
      if (h0_c != 0) {
      h2_pper.SetBinContent(i, h1_c/h0_c);
      h2_pper.SetBinError(i, sqrt(pow(h1_e/h0_c,2.0)+pow(h1_c*h0_e/(h0_c*h0_c),2.0)));
      }
      else {
      h2_pper.SetBinContent(i, 0);
      h2_pper.SetBinError(i, 0);
      }

      h0_c=h0_x0->GetBinContent(i);
      h0_e=h0_x0->GetBinError(i);
      h1_c=h1_x0->GetBinContent(i);
      h1_e=h1_x0->GetBinError(i);
      if (h0_c != 0) {
      h2_x0.SetBinContent(i, h1_c/h0_c);
      h2_x0.SetBinError(i, sqrt(pow(h1_e/h0_c,2.0)+pow(h1_c*h0_e/(h0_c*h0_c),2.0)));
      }
      else {
      h2_x0.SetBinContent(i, 0);
      h2_x0.SetBinError(i, 0);
      }

      h0_c=h0_y0->GetBinContent(i);
      h0_e=h0_y0->GetBinError(i);
      h1_c=h1_y0->GetBinContent(i);
      h1_e=h1_y0->GetBinError(i);
      if (h0_c != 0) {
      h2_y0.SetBinContent(i, h1_c/h0_c);
      h2_y0.SetBinError(i, sqrt(pow(h1_e/h0_c,2.0)+pow(h1_c*h0_e/(h0_c*h0_c),2.0)));
      }
      else {
      h2_y0.SetBinContent(i, 0);
      h2_y0.SetBinError(i, 0);
      }
    }
  for (int i=1; i<=30; i++)
    {
      h0_c=h0_phi0->GetBinContent(i);
      h0_e=h0_phi0->GetBinError(i);
      h1_c=h1_phi0->GetBinContent(i);
      h1_e=h1_phi0->GetBinError(i);
      if (h0_c != 0) {
      h2_phi0.SetBinContent(i, h1_c/h0_c);
      h2_phi0.SetBinError(i, sqrt(pow(h1_e/h0_c,2.0)+pow(h1_c*h0_e/(h0_c*h0_c),2.0)));
      }
      else {
      h2_phi0.SetBinContent(i, 0);
      h2_phi0.SetBinError(i, 0);
      }

      h0_c=h0_lambda->GetBinContent(i);
      h0_e=h0_lambda->GetBinError(i);
      h1_c=h1_lambda->GetBinContent(i);
      h1_e=h1_lambda->GetBinError(i);
      if (h0_c != 0) {
      h2_lambda.SetBinContent(i, h1_c/h0_c);
      h2_lambda.SetBinError(i, sqrt(pow(h1_e/h0_c,2.0)+pow(h1_c*h0_e/(h0_c*h0_c),2.0)));
      }
      else {
      h2_lambda.SetBinContent(i, 0);
      h2_lambda.SetBinError(i, 0);
      }

    }

  TCanvas *c1 = new TCanvas("c1","",800,600);
  c1->cd();
  h2_pper.GetXaxis()->SetTitle("p_{T, truth}");
  h2_pper.GetYaxis()->SetTitle("Track Reconstruction Efficiency");
  h2_pper.GetYaxis()->SetRangeUser(0,1.4);
  h2_pper.Draw("L,P,E1");
  ATLASLabel(0.2,0.85,"Internal");
  c1->Update();
  c1->SaveAs(Form("plots/%s/efficiency_pper.png",name.Data()));
  h2_x0.GetXaxis()->SetTitle("x_{0, truth}");
  h2_x0.GetYaxis()->SetTitle("Track Reconstruction Efficiency");
  h2_x0.GetYaxis()->SetRangeUser(0,1.4);
  h2_x0.Draw("L,P,E1");
  ATLASLabel(0.2,0.85,"Internal");
  c1->Update();
  c1->SaveAs(Form("plots/%s/efficiency_x0.png",name.Data()));
  h2_y0.GetXaxis()->SetTitle("y_{0, truth}");
  h2_y0.GetYaxis()->SetTitle("Track Reconstruction Efficiency");
  h2_y0.GetYaxis()->SetRangeUser(0,1.4);
  h2_y0.Draw("L,P,E1");
  ATLASLabel(0.2,0.85,"Internal");
  c1->Update();
  c1->SaveAs(Form("plots/%s/efficiency_y0.png",name.Data()));
  h2_phi0.GetXaxis()->SetTitle("#phi_{0, truth}");
  h2_phi0.GetYaxis()->SetTitle("Track Reconstruction Efficiency");
  h2_phi0.GetYaxis()->SetRangeUser(0,1.4);
  h2_phi0.Draw("L,P,E1");
  ATLASLabel(0.2,0.85,"Internal");
  c1->Update();
  c1->SaveAs(Form("plots/%s/efficiency_phi0.png",name.Data()));
  h2_lambda.GetXaxis()->SetTitle("#lambda_{truth}");
  h2_lambda.GetYaxis()->SetTitle("Track Reconstruction Efficiency");
  h2_lambda.GetYaxis()->SetRangeUser(0,1.4);
  h2_lambda.Draw("L,P,E1");
  ATLASLabel(0.2,0.85,"Internal");
  c1->Update();
  c1->SaveAs(Form("plots/%s/efficiency_lambda.png",name.Data()));
  
  f->cd();
  h2_pper.Write();
  h2_x0.Write();
  h2_y0.Write();
  h2_phi0.Write();
  h2_lambda.Write();
  //f->Close();
#endif

}
