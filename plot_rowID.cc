#include "TTree.h"
#include "TProfile.h"
#include "AtlasLabels.h"
#include "AtlasLabels.C"
#include "AtlasStyle.h"
#include "AtlasStyle.C"
#include "AtlasUtils.h"
#include "AtlasUtils.C"

void plot_rowID()
{
  SetAtlasStyle();

  TString name="9plane_muMinus";
  const int N_plane=9;
  //double target_z[8]={0, 50, 909.04, 959.04, 1009.04, 1059.04, 1942.02, 1992.02};
  double target_z[9]={0, 50, 100, 938.03, 988.03, 1038.03, 1892.02, 1942.02, 1992.02};

  //TFile *f = new TFile("helix_tracker_output_242_muMinus.root");
  TFile *f = new TFile("helix_tracker_output_333_muMinus.root");
  TTree *t = (TTree*)f->Get("tracks");
  double truth_px, truth_py, truth_pz, truth_R, truth_phi0, truth_lambda, reco_px, reco_py, reco_pz, reco_R, reco_phi0, reco_lambda;
  double chi_square;
  int pdgID, N_track_points;
    std::vector<double> *space_points_x;
    std::vector<double> *space_points_y;
    std::vector<double> *space_points_z;
    std::vector<double> *track_points_x;
    std::vector<double> *track_points_y;
    std::vector<double> *track_points_z;
    std::vector<int> *track_points_rowID;
    std::vector<double> *truth_x;
    std::vector<double> *truth_y;
    std::vector<double> *truth_z;
    std::vector<double> *reco_x;
    std::vector<double> *reco_y;
    std::vector<double> *reco_z;
  t->SetBranchAddress("chi_square", &chi_square);
  t->SetBranchAddress("pdgID", &pdgID);
  t->SetBranchAddress("N_track_points", &N_track_points);
  t->SetBranchAddress("truth_px", &truth_px);
  t->SetBranchAddress("truth_py", &truth_py);
  t->SetBranchAddress("truth_pz", &truth_pz);
  t->SetBranchAddress("truth_R", &truth_R);
  t->SetBranchAddress("truth_phi0", &truth_phi0);
  t->SetBranchAddress("truth_lambda", &truth_lambda);
  t->SetBranchAddress("reco_px", &reco_px);
  t->SetBranchAddress("reco_py", &reco_py);
  t->SetBranchAddress("reco_pz", &reco_pz);
  t->SetBranchAddress("reco_R", &reco_R);
  t->SetBranchAddress("reco_phi0", &reco_phi0);
  t->SetBranchAddress("reco_lambda", &reco_lambda);
  t->SetBranchAddress("truth_x", &truth_x);
  t->SetBranchAddress("truth_y", &truth_y);
  t->SetBranchAddress("truth_z", &truth_z);
  t->SetBranchAddress("reco_x", &reco_x);
  t->SetBranchAddress("reco_y", &reco_y);
  t->SetBranchAddress("reco_z", &reco_z);
  t->SetBranchAddress("space_points_x", &space_points_x);
  t->SetBranchAddress("space_points_y", &space_points_y);
  t->SetBranchAddress("space_points_z", &space_points_z);
  t->SetBranchAddress("track_points_x", &track_points_x);
  t->SetBranchAddress("track_points_y", &track_points_y);
  t->SetBranchAddress("track_points_z", &track_points_z);
  t->SetBranchAddress("track_points_rowID", &track_points_rowID);

  TFile *f_out = new TFile(Form("output/%s/rowID_validation.root",name.Data()),"recreate");
  TH2D *h_truth_residual_X = new TH2D("truth_residual_X","",16*N_plane,0,16*N_plane,1000,-100,100);
  TH2D *h_truth_residual_Y = new TH2D("truth_residual_Y","",16*N_plane,0,16*N_plane,500,-100,100);
  TH2D *h_biased_residual_X = new TH2D("biased_residual_X","",16*N_plane,0,16*N_plane,1000,-100,100);
  TH2D *h_biased_residual_Y = new TH2D("biased_residual_Y","",16*N_plane,0,16*N_plane,500,-100,100);

  int entries=t->GetEntries();
  cout<<"total entries in tree = "<<entries<<endl;
  for (int iEntry=0; iEntry<entries; iEntry++)
    {
      t->GetEntry(iEntry);
      //cout<<track_points_x->size()<<endl;
      for (int i=0; i<track_points_x->size(); i++)
        {
          for (int j=0; j<N_plane; j++)
	    {
              if (fabs(track_points_z->at(i)-target_z[j]) < 1) 
	       {
		  h_truth_residual_X->Fill(track_points_rowID->at(i), track_points_x->at(i)-truth_x->at(j));
		  h_truth_residual_Y->Fill(track_points_rowID->at(i), track_points_y->at(i)-truth_y->at(j));
		  h_biased_residual_X->Fill(track_points_rowID->at(i), track_points_x->at(i)-reco_x->at(j));
		  h_biased_residual_Y->Fill(track_points_rowID->at(i), track_points_y->at(i)-reco_y->at(j));
	       }
	    }
	}
    }

  TProfile *p_truth_residual_X = h_truth_residual_X->ProfileX("truth_residual_X_profile");
  TProfile *p_truth_residual_Y = h_truth_residual_Y->ProfileX("truth_residual_Y_profile");
  TProfile *p_biased_residual_X = h_biased_residual_X->ProfileX("biased_residual_X_profile");
  TProfile *p_biased_residual_Y = h_biased_residual_Y->ProfileX("biased_residual_Y_profile");
  TH1D *h_truth_residual_X_RMS = new TH1D("truth_residual_X_RMS","",16*N_plane,0,16*N_plane);
  TH1D *h_truth_residual_Y_RMS = new TH1D("truth_residual_Y_RMS","",16*N_plane,0,16*N_plane);
  TH1D *h_biased_residual_X_RMS = new TH1D("biased_residual_X_RMS","",16*N_plane,0,16*N_plane);
  TH1D *h_biased_residual_Y_RMS = new TH1D("biased_residual_Y_RMS","",16*N_plane,0,16*N_plane);
  for (int i=1; i<=16*N_plane; i++)
    {
     // if (p_truth_residual_X->GetBinContent(i) != 0) 
      {
      h_truth_residual_X_RMS->SetBinContent(i,p_truth_residual_X->GetBinError(i));
      h_truth_residual_Y_RMS->SetBinContent(i,p_truth_residual_Y->GetBinError(i));
      h_biased_residual_X_RMS->SetBinContent(i,p_biased_residual_X->GetBinError(i));
      h_biased_residual_Y_RMS->SetBinContent(i,p_biased_residual_Y->GetBinError(i));
      }
    }

  double ymax=0, ymin=0;
  TCanvas *c1 = new TCanvas("c1","",800,600);
  c1->cd();

  h_truth_residual_X->GetXaxis()->SetTitle("rowID");
  h_truth_residual_X->GetYaxis()->SetTitle("x_{Space Point} - x_{Truth} [mm]");
  h_truth_residual_X->SetMarkerSize(0.5);
  h_truth_residual_X->Draw();
  c1->Update();
  c1->SaveAs(Form("plots/%s/truth_residual_X.png",name.Data()));

  h_truth_residual_Y->GetXaxis()->SetTitle("rowID");
  h_truth_residual_Y->GetYaxis()->SetTitle("y_{Space Point} - y_{Truth} [mm]");
  h_truth_residual_Y->SetMarkerSize(0.5);
  h_truth_residual_Y->Draw();
  c1->Update();
  c1->SaveAs(Form("plots/%s/truth_residual_Y.png",name.Data()));

  h_biased_residual_X->GetXaxis()->SetTitle("rowID");
  h_biased_residual_X->GetYaxis()->SetTitle("x_{Space Point} - x_{Reco} [mm]");
  h_biased_residual_X->SetMarkerSize(0.5);
  h_biased_residual_X->Draw();
  c1->Update();
  c1->SaveAs(Form("plots/%s/biased_residual_X.png",name.Data()));

  h_biased_residual_Y->GetXaxis()->SetTitle("rowID");
  h_biased_residual_Y->GetYaxis()->SetTitle("y_{Space Point} - y_{Reco} [mm]");
  h_biased_residual_Y->SetMarkerSize(0.5);
  h_biased_residual_Y->Draw();
  c1->Update();
  c1->SaveAs(Form("plots/%s/biased_residual_Y.png",name.Data()));

  p_truth_residual_X->GetXaxis()->SetTitle("rowID");
  p_truth_residual_X->GetYaxis()->SetTitle("x_{Space Point} - x_{Truth} [mm]");
  ymax = p_truth_residual_X->GetMaximum();
  ymin = p_truth_residual_X->GetMinimum();
  p_truth_residual_X->GetYaxis()->SetRangeUser(ymin-fabs(0.5*ymax), 1.5*ymax);
  p_truth_residual_X->Draw();
  ATLASLabel(0.2,0.85,"Internal");
  c1->Update();
  c1->SaveAs(Form("plots/%s/truth_residual_X_profile.png",name.Data()));

  p_truth_residual_Y->GetXaxis()->SetTitle("rowID");
  p_truth_residual_Y->GetYaxis()->SetTitle("y_{Space Point} - y_{Truth} [mm]");
  ymax = p_truth_residual_Y->GetMaximum();
  ymin = p_truth_residual_Y->GetMinimum();
  p_truth_residual_Y->GetYaxis()->SetRangeUser(ymin-fabs(0.5*ymax), 1.5*ymax);
  p_truth_residual_Y->Draw();
  ATLASLabel(0.2,0.85,"Internal");
  c1->Update();
  c1->SaveAs(Form("plots/%s/truth_residual_Y_profile.png",name.Data()));

  p_biased_residual_X->GetXaxis()->SetTitle("rowID");
  p_biased_residual_X->GetYaxis()->SetTitle("x_{Space Point} - x_{Reco} [mm]");
  ymax = p_biased_residual_X->GetMaximum();
  ymin = p_biased_residual_X->GetMinimum();
  p_biased_residual_X->GetYaxis()->SetRangeUser(ymin-fabs(0.5*ymax), 1.5*ymax);
  p_biased_residual_X->Draw();
  ATLASLabel(0.2,0.85,"Internal");
  c1->Update();
  c1->SaveAs(Form("plots/%s/biased_residual_X_profile.png",name.Data()));

  p_biased_residual_Y->GetXaxis()->SetTitle("rowID");
  p_biased_residual_Y->GetYaxis()->SetTitle("y_{Space Point} - y_{Reco} [mm]");
  ymax = p_biased_residual_Y->GetMaximum();
  ymin = p_biased_residual_Y->GetMinimum();
  p_biased_residual_Y->GetYaxis()->SetRangeUser(ymin-fabs(0.5*ymax), 1.5*ymax);
  p_biased_residual_Y->Draw();
  ATLASLabel(0.2,0.85,"Internal");
  c1->Update();
  c1->SaveAs(Form("plots/%s/biased_residual_Y_profile.png",name.Data()));

  h_truth_residual_X_RMS->GetXaxis()->SetTitle("rowID");
  h_truth_residual_X_RMS->GetYaxis()->SetTitle("RMS(x_{Space Point} - x_{Truth}) [mm]");
  ymax = h_truth_residual_X_RMS->GetMaximum();
  ymin = h_truth_residual_X_RMS->GetMinimum();
  h_truth_residual_X_RMS->GetYaxis()->SetRangeUser(0, 1.5*ymax);
  h_truth_residual_X_RMS->Draw("P");
  ATLASLabel(0.2,0.85,"Internal");
  c1->Update();
  c1->SaveAs(Form("plots/%s/truth_residual_X_RMS.png",name.Data()));

  h_truth_residual_Y_RMS->GetXaxis()->SetTitle("rowID");
  h_truth_residual_Y_RMS->GetYaxis()->SetTitle("RMS(y_{Space Point} - y_{Truth}) [mm]");
  ymax = h_truth_residual_Y_RMS->GetMaximum();
  ymin = h_truth_residual_Y_RMS->GetMinimum();
  h_truth_residual_Y_RMS->GetYaxis()->SetRangeUser(0, 1.5*ymax);
  h_truth_residual_Y_RMS->Draw("P");
  ATLASLabel(0.2,0.85,"Internal");
  c1->Update();
  c1->SaveAs(Form("plots/%s/truth_residual_Y_RMS.png",name.Data()));

  h_biased_residual_X_RMS->GetXaxis()->SetTitle("rowID");
  h_biased_residual_X_RMS->GetYaxis()->SetTitle("RMS(x_{Space Point} - x_{Truth}) [mm]");
  ymax = h_biased_residual_X_RMS->GetMaximum();
  ymin = h_biased_residual_X_RMS->GetMinimum();
  h_biased_residual_X_RMS->GetYaxis()->SetRangeUser(0, 1.5*ymax);
  h_biased_residual_X_RMS->Draw("P");
  ATLASLabel(0.2,0.85,"Internal");
  c1->Update();
  c1->SaveAs(Form("plots/%s/biased_residual_X_RMS.png",name.Data()));

  h_biased_residual_Y_RMS->GetXaxis()->SetTitle("rowID");
  h_biased_residual_Y_RMS->GetYaxis()->SetTitle("RMS(y_{Space Point} - y_{Truth}) [mm]");
  ymax = h_biased_residual_Y_RMS->GetMaximum();
  ymin = h_biased_residual_Y_RMS->GetMinimum();
  h_biased_residual_Y_RMS->GetYaxis()->SetRangeUser(0, 1.5*ymax);
  h_biased_residual_Y_RMS->Draw("P");
  ATLASLabel(0.2,0.85,"Internal");
  c1->Update();
  c1->SaveAs(Form("plots/%s/biased_residual_Y_RMS.png",name.Data()));

  f_out->cd();
  h_truth_residual_X->Write();
  h_truth_residual_Y->Write();
  h_biased_residual_X->Write();
  h_biased_residual_Y->Write();
  f_out->Close();
}
